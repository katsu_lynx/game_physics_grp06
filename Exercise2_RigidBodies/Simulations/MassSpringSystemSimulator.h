#ifndef MASSSPRINGSYSTEMSIMULATOR_h
#define MASSSPRINGSYSTEMSIMULATOR_h
#include "Simulator.h"
#include "Objects.h"

// Do Not Change
#define EULER 0
#define LEAPFROG 1
#define MIDPOINT 2
// Do Not Change

class MassSpringSystemSimulator:public Simulator{
public:
	//Construtors
	MassSpringSystemSimulator();
	
	// Functions
	const char * getTestCasesStr();
	void initUI(DrawingUtilitiesClass * DUC);
	void reset();
	void drawFrame(ID3D11DeviceContext* pd3dImmediateContext);
	void notifyCaseChanged(int testCase); 
	void externalForcesCalculations(float timeElapsed);
	void simulateTimestep(float timeStep);
	void onClick(int x, int y);
	void onMouse(int x, int y);
	// Specific Functions
	void setIntegrator(int integrator);
	void setMass(float mass);
	void setStiffness(float stiffness);
	void setDampingFactor(float damping);
	int addMassPoint(Vec3 position, Vec3 Velocity, bool isFixed);
	int addMassPoint(Vec3 position, Vec3 Velocity, bool isFixed, int mass);
	void addSpring(int masspoint1, int masspoint2, float initialLength);
	int getNumberOfMassPoints();
	int getNumberOfSprings();
	Vec3 getPositionOfMassPoint(int index);
	Vec3 getVelocityOfMassPoint(int index);
	void applyExternalForce(Vec3 force);
	void doEuler(float timeStep);
	void doMidpoint(float timeStep);
	void calculateFloorCollision(Masspoint * masspoint);

private:
	// Data Attributes
	float m_fMass;
	float m_fStiffness = 40.0f;
	float m_fDamping = 1.0f;
	float m_fGravity = 9.81;
	int m_iIntegrator = 0;
	bool interaction = 0;
	bool gravity = 0;
	bool collisions = 0;
	// UI Attributes
	Vec3 m_externalForce = Vec3(0,0,0);
	Point2D m_mouse;
	Point2D m_trackmouse;
	Point2D m_oldtrackmouse;

	// Objects
	std::vector<Masspoint> masspoints;
	std::vector<Spring> springs;
};
#endif