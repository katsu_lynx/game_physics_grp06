#include "RigidBodySystemSimulator.h"
#include "collisionDetect.h"


RigidBodySystemSimulator::RigidBodySystemSimulator() {
	m_iTestCase = 0;
	this->gravity = false;
}

void RigidBodySystemSimulator::addRigidBody(Vec3 position, Vec3 size, int mass) {
	this->m_pRigidBodySystem.push_back(RigidBody(position, size, mass));
}

void RigidBodySystemSimulator::applyForceOnBody(int i, Vec3 loc, Vec3 force) {
	RigidBody * body = &this->m_pRigidBodySystem.at(i);
	
	

	XMVECTOR compare = XMVectorEqual(loc.toDirectXVector(), body->getPosition().toDirectXVector());
	if (!compare.m128_f32[0] || !compare.m128_f32[1] || !compare.m128_f32[2]) {
		float length = sqrt(loc.squaredDistanceTo(body->getPosition()));
		for (int i = 0; i < 8; i++) {
			Vec3 vertexRealPos = body->vertices.at(i) + body->getPosition();
			float percentage = sqrt(loc.squaredDistanceTo(vertexRealPos)) / length;
			if (percentage > 1.0f) {
				percentage = 1.0f - percentage;
			}
			body->vertexForces.at(i) +=  force * percentage;
		}
	}
	else {
		body->setForceAccumulator(body->getForceAccumulator() + force);
	}
}

Mat4 calculateWorldPosition(RigidBody body) {
	// Scale Matrix
	Vec3 scaleVec = body.getSize().toDirectXVector();
	Mat4 scale = Mat4(
		scaleVec.x, 0.0f, 0.0f, 0.0f,
		0.0f, scaleVec.y, 0.0f, 0.0f,
		0.0f, 0.f, scaleVec.z, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	);

	// Rotation Matrix
	Quat o = body.getOrientation();
	double cx = cos(o.x);
	double sx = sin(o.x);
	double cy = cos(o.y);
	double sy = sin(o.y);
	double cz = cos(o.z);
	double sz = sin(o.z);
	Mat4 rotation = Mat4(1.0f, 0.0f, 0.0f, 0.0f, 0.0f, cos(o.x), sin(o.x), 0.0f, 0.0f, -sin(o.x), cos(o.x), 0.0f, 0.0f, 0.0f, 0.0f, 1.0f) *
		Mat4(cos(o.y), 0.0f, -sin(o.y), 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, sin(o.y), 0.0f, cos(o.y), 0.0f, 0.0f, 0.0f, 0.0f, 1.0f) *
		Mat4(cos(o.z), sin(o.z), 0.0f, 0.0f, -sin(o.z), cos(o.z), 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f);


	// Translation Matrix
	Vec3 transVec = body.getPosition();
	Mat4 translation = Mat4(
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		transVec.x, transVec.y, transVec.z, 1.0f
	);

	// Object World Matrix
	return scale * rotation * translation;
}

void RigidBodySystemSimulator::drawFrame(ID3D11DeviceContext* pd3dImmediateContext) {
	std::mt19937 eng;
	std::uniform_real_distribution<float> randCol(0.0f, 1.0f);
	for (RigidBody body : this->m_pRigidBodySystem) {
		

		// Draw Object
		DUC->setUpLighting(Vec3(), 0.4*Vec3(1, 1, 1), 100, 0.6*Vec3(randCol(eng), randCol(eng), randCol(eng)));
		DUC->drawRigidBody(calculateWorldPosition(body));
	}
}

void RigidBodySystemSimulator::externalForcesCalculations(float timeElapsed) {
	Vec3 forces = Vec3(0, 0, 0);
	if (this->interaction && (m_trackmouse.x != 0 || m_trackmouse.y != 0)) {
		// Calcuate camera directions in world space
		Mat4 m = Mat4(DUC->g_camera.GetViewMatrix());
		m = m.inverse();
		Vec3 camRightWorld = Vec3(g_XMIdentityR0);
		camRightWorld = m.transformVectorNormal(camRightWorld);
		Vec3 camUpWorld = Vec3(g_XMIdentityR1);
		camUpWorld = m.transformVectorNormal(camUpWorld);

		// Add accumulated mouse deltas to movable object pos
		float speedScale = 2.0f;
		forces += speedScale * 0.5 * (float)m_trackmouse.x * camRightWorld;
		forces += speedScale * m_fGravity * (float)m_trackmouse.y * camUpWorld;

		// Reset accumulated mouse deltas
		m_trackmouse.x = m_trackmouse.y = 0;
	}

	for (int i = 0; i < m_pRigidBodySystem.size(); i++) {
		RigidBody * body = &m_pRigidBodySystem.at(i);

		this->applyForceOnBody(i, Vec3(0, 0, 0), forces);

		Vec3 acceleration = body->getForceAccumulator() * body->getMassInverse();
		body->setVelocity(body->getVelocity() + acceleration * timeElapsed);

		Vec3 angularAcceleration = body->getTorqueAccumulator() * body->getMassInverse();
		body->setAngularVelocity(body->getAngularVelocity() + angularAcceleration * timeElapsed);
	}
}

Vec3 RigidBodySystemSimulator::getAngularVelocityOfRigidBody(int i) {
	RigidBody * body = &this->m_pRigidBodySystem.at(i);
	return body->getAngularVelocity();
}

Vec3 RigidBodySystemSimulator::getLinearVelocityOfRigidBody(int i) {
	RigidBody * body = &this->m_pRigidBodySystem.at(i);
	return body->getVelocity();
}

int RigidBodySystemSimulator::getNumberOfRigidBodies() {
	return this->m_pRigidBodySystem.size();
}

Vec3 RigidBodySystemSimulator::getPositionOfRigidBody(int i) {
	RigidBody * body = &this->m_pRigidBodySystem.at(i);
	return body->getPosition();
}

const char * RigidBodySystemSimulator::getTestCasesStr() {
	return "Demo1: a simple one-step test,Demo2: a simple single body simulation,Demo3: a two-rigid-body collision scene,Demo4: a complex simulation";
}

void RigidBodySystemSimulator::initUI(DrawingUtilitiesClass * DUC) {
	cout << "Test Case " << m_iTestCase;
	this->DUC = DUC;
	switch (m_iTestCase) {
	case 0:break;
	case 1:break;
	case 2:break;
	case 3:break;
	}
}

void RigidBodySystemSimulator::notifyCaseChanged(int testCase) {
	this->gravity = false;
	this->interaction = false;
	this->m_pRigidBodySystem.clear();
	m_iTestCase = testCase;
	Quat o = Quat();
	Vec3 x = Vec3();
	Vec3 v = Vec3();
	Mat4 rot = Mat4();
	switch (m_iTestCase) {
	case 0:
		this->addRigidBody(Vec3(0.0f, 0.0f, 0.0f), Vec3(1.0f, 0.6f, 0.5f), 2.0f);
		this->setOrientationOf(0, Quat(0.0f, 0.0f, 0.5*M_PI, 1.0f));
		this->applyForceOnBody(0, Vec3(0.3f, 0.5f, 0.25f), Vec3(-1.0f, -1.0f, 0.0f));
		this->simulateTimestep(2.0f);

		cout << "Demo1: a simple one-step test !\n";
		cout << "Linear Velocity: " << this->m_pRigidBodySystem.at(0).getVelocity().toString() << endl;
		cout << "Angular Velocity: " << this->m_pRigidBodySystem.at(0).getAngularVelocity().toString() << endl;
		o = this->m_pRigidBodySystem.at(0).getOrientation();
		rot = Mat4(1.0f, 0.0f, 0.0f, 0.0f, 0.0f, cos(o.x), sin(o.x), 0.0f, 0.0f, -sin(o.x), cos(o.x), 0.0f, 0.0f, 0.0f, 0.0f, 1.0f) *
			Mat4(cos(o.y), 0.0f, -sin(o.y), 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, sin(o.y), 0.0f, cos(o.y), 0.0f, 0.0f, 0.0f, 0.0f, 1.0f) *
			Mat4(cos(o.z), sin(o.z), 0.0f, 0.0f, -sin(o.z), cos(o.z), 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f);
		x = this->m_pRigidBodySystem.at(0).getPosition()+rot*(this->m_pRigidBodySystem.at(0).vertices.at(7));
		v = this->m_pRigidBodySystem.at(0).getVelocity() + this->m_pRigidBodySystem.at(0).getInertiaTensorInverse()*this->m_pRigidBodySystem.at(0).getAngularMomentum()*x;
		cout << "Point World Velocity: " << v.toString() << endl;

		break;
	case 1:
		this->addRigidBody(Vec3(0.0f, 0.0f, 0.0f), Vec3(1.0f, 0.6f, 0.5f), 2.0f);
		this->setOrientationOf(0, Quat(0.0f,0.0f,0.5*M_PI,1.0f));
		this->applyForceOnBody(0, Vec3(0.3f, 0.5f, 0.25f), Vec3(-1.0f, -1.0f, 0.0f));
		this->interaction = true;
		cout << "Demo2: a simple single body simulation !\n";
		break;
	case 2:
		cout << "Demo3: a two-rigid-body collision scene !\n";
		this->addRigidBody(Vec3(1.0f, 0.0f, 0.0f), Vec3(1.0f, 0.6f, 0.5f), 2.0f);
		this->addRigidBody(Vec3(-1.0f, 0.0f, 0.0f), Vec3(1.0f, 0.6f, 0.5f), 2.0f);
		this->setOrientationOf(0, Quat(0.0f, 0.2*M_PI, 0.2*M_PI, 1.0f));
		this->applyForceOnBody(0, Vec3(1.0f, 0.0f, 0.0f), Vec3(-1.0f, 0.0f, 0.0f));
		this->applyForceOnBody(1, Vec3(-1.0f, 0.0f, 0.0f), Vec3(1.0f, 0.0f, 0.0f));
		
		break;
	case 3:
		cout << "Demo4: a complex simulation !\n";
		this->addRigidBody(Vec3(1.0f, 0.0f, 0.0f), Vec3(1.0f, 0.6f, 0.5f), 2.0f);
		this->addRigidBody(Vec3(-1.0f, 0.0f, 0.0f), Vec3(1.0f, 0.6f, 0.5f), 2.0f);
		this->addRigidBody(Vec3(0.0f, 1.0f, 0.0f), Vec3(1.0f, 0.6f, 0.5f), 2.0f);
		this->addRigidBody(Vec3(0.0f, -1.0f, 0.0f), Vec3(1.0f, 0.6f, 0.5f), 2.0f);
		this->gravity = true;

		break;
	}
}

void RigidBodySystemSimulator::onClick(int x, int y) {
	m_trackmouse.x += x - m_oldtrackmouse.x;
	m_trackmouse.y += y - m_oldtrackmouse.y;
}

void RigidBodySystemSimulator::onMouse(int x, int y) {
	m_oldtrackmouse.x = x;
	m_oldtrackmouse.y = y;
}

void RigidBodySystemSimulator::reset() {
	m_mouse.x = m_mouse.y = 0;
	m_trackmouse.x = m_trackmouse.y = 0;
	m_oldtrackmouse.x = m_oldtrackmouse.y = 0;
	this->notifyCaseChanged(this->m_iTestCase);
	this->interaction = false;
	this->gravity = false;
}

void RigidBodySystemSimulator::setOrientationOf(int i, Quat orientation) {
	RigidBody * body = &this->m_pRigidBodySystem.at(i);
	body->setOrientation(orientation);
}

void RigidBodySystemSimulator::setVelocityOf(int i, Vec3 velocity) {
	RigidBody * body = &this->m_pRigidBodySystem.at(i);
	body->setVelocity(velocity);
}

void RigidBodySystemSimulator::simulateTimestep(float timeStep) {
	this->externalForcesCalculations(timeStep);

	/* Calculate current positions of rigid bodies for collision detection */
	std:vector<Mat4> worldPositions;
	for (int i = 0; i < m_pRigidBodySystem.size(); i++) {
		worldPositions.push_back(calculateWorldPosition(m_pRigidBodySystem.at(i)));
	}

	for (int i = 0; i < m_pRigidBodySystem.size(); i++) {
		RigidBody * body = &m_pRigidBodySystem.at(i);

		/* Collision Detection */
		for (int j = i+1; j < m_pRigidBodySystem.size(); j++) {
				CollisionInfo collision = checkCollisionSAT(worldPositions.at(i), worldPositions.at(j));
				if (collision.isValid) {
					cout << "Collision!" << endl;
					Vec3 relPos = body->getVelocity() - this->m_pRigidBodySystem.at(j).getVelocity();
					if (sqrt((relPos * collision.normalWorld).squaredDistanceTo(Vec3())) >= 0.0f) {
						this->applyForceOnBody(i, collision.collisionPointWorld, -collision.normalWorld);
						this->applyForceOnBody(j, collision.collisionPointWorld, collision.normalWorld);
					}
				}
		}
		CollisionInfo floorCollision = checkCollisionSAT(worldPositions.at(i), worldFloor);
		if (floorCollision.isValid) {
			body->setVelocity(Vec3(body->getVelocity().x, 0.0f, body->getVelocity().z));
			if (body->getPosition().y < 0.0f) {
				Vec3 position = body->getPosition();
				position.y += timeStep;
				body->setPosition(position);
			}
			Quat o = body->getOrientation();

			if (std::fmod(sin(o.x), 1.0f) > 0) {
				Quat orientation = body->getOrientation();
				double missing = (double)((int)(orientation.x / M_PI_2)) * M_PI - orientation.x;
				body->setOrientation(Quat(orientation.x - missing * min(1.0, 90.0 * timeStep), orientation.y, orientation.z, orientation.w));
			}
			if (std::fmod(sin(o.y), 1.0f) > 0) {
				Quat orientation = body->getOrientation();
				double missing = (double)((int)(orientation.y / M_PI_2)) * M_PI - orientation.y;
				body->setOrientation(Quat(orientation.x, orientation.y - missing * min(1.0, 90.0 * timeStep), orientation.z, orientation.w));
			}
			if (std::fmod(sin(o.z), 1.0f) > 0) {
				Quat orientation = body->getOrientation();
				double missing = (double)((int)(orientation.z / M_PI_2)) * M_PI - orientation.z;
				body->setOrientation(Quat(orientation.x, orientation.y, orientation.z - missing * min(1.0, 90.0 * timeStep), orientation.w));
			}
		} else if (this->gravity) {
			this->applyForceOnBody(i, body->getPosition(), Vec3(0, this->m_fGravity, 0));
		}

		Vec3 q = Vec3();
		Vec3 f = Vec3();

		for (int i = 0; i < 8; i++) {
			Vec3 vertex = body->vertices.at(i);
			Vec3 force = body->vertexForces.at(i);
			body->vertexForces.at(i) = Vec3();
			q += Vec3(vertex.y*force.z - vertex.z*force.y, vertex.z*force.x - vertex.x*force.z, vertex.x*force.y - vertex.y*force.x);
			f += force;
		}

		// Euler Velocity & Position
		body->setPosition(body->getPosition() + timeStep * body->getVelocity());
		body->setVelocity(body->getVelocity() + timeStep * f * body->getMassInverse());

		// Orientation
		Quat quat = Quat();
		quat.x = body->getAngularVelocity().x;
		quat.y = body->getAngularVelocity().y;
		quat.z = body->getAngularVelocity().z;
		body->setOrientation(body->getOrientation() + timeStep / 2.0f * quat);

		// Angular Momentum
		body->setAngularMomentum(body->getAngularMomentum() + timeStep * q);

		// Inertia Tensor
		Quat o = body->getOrientation();
		Mat4 rot = Mat4(1.0f, 0.0f, 0.0f, 0.0f, 0.0f, cos(o.x), sin(o.x), 0.0f, 0.0f, -sin(o.x), cos(o.x), 0.0f, 0.0f, 0.0f, 0.0f, 1.0f) *
					Mat4(cos(o.y), 0.0f, -sin(o.y), 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, sin(o.y), 0.0f, cos(o.y), 0.0f, 0.0f, 0.0f, 0.0f, 1.0f) *
					Mat4(cos(o.z), sin(o.z), 0.0f, 0.0f, -sin(o.z), cos(o.z), 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f);
		Mat4 rotT = rot;
		rotT.transpose();
		body->setInertiaTensorInverse(rot * body->getInertiaTensorInverse() * rotT);

		//Factor to show rotation
		float fac = 100.0f;
		// Angular Velocity
		body->setAngularVelocity(body->getInertiaTensorInverse() * body->getAngularMomentum() * fac);
	}
}