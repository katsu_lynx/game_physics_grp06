#pragma once
#include "Simulator.h"

class Masspoint {
public:
	Masspoint() {}
	Masspoint(Vec3 position, Vec3 velocity, bool isFixed) {
		this->position = position;
		this->velocity = velocity;
		this->isFixed = isFixed;
	}
	Masspoint(Vec3 position, Vec3 velocity, bool isFixed, float mass) {
		this->position = position;
		this->velocity = velocity;
		this->isFixed = isFixed;
		this->mass = mass;
	}
	void setPosition(Vec3 position) {
		this->position = position;
	}
	Vec3 getPosition() {
		return this->position;
	}
	void setVelocity(Vec3 velocity) {
		this->velocity = velocity;
	}
	Vec3 getVelocity() {
		return this->velocity;
	}
	void setAcceleration(Vec3 acceleration) {
		this->acceleration = acceleration;
	}
	Vec3 getAcceleration() {
		return this->acceleration;
	}
	void setIsFixed(bool fixed) {
		this->isFixed = isFixed;
	}
	bool getIsFixed() {
		return this->isFixed;
	}
	void setMass(float mass) {
		this->mass = mass;
	}
	float getMass() {
		return this->mass;
	}

private:
	Vec3 position;
	Vec3 velocity;
	Vec3 acceleration;
	bool isFixed;
	float mass;
};

class Spring {
public:
	Spring() {}
	Spring(Masspoint masspoint1, Masspoint masspoint2, float initalLength) {
		this->masspoint1 = masspoint1;
		this->masspoint2 = masspoint2;
		this->initialLength = initialLength;
	}
	Spring(Masspoint masspoint1, Masspoint masspoint2, float initalLength, float stifness) {
		this->masspoint1 = masspoint1;
		this->masspoint2 = masspoint2;
		this->initialLength = initialLength;
		this->stiffness = stiffness;
	}
	Spring(int massIndex1, int massIndex2, float initialLength) {
		this->massIndex1 = massIndex1;
		this->massIndex2 = massIndex2;
		this->initialLength = initialLength;
	}
	void setMasspoint1(Masspoint masspoint) {
		this->masspoint1 = masspoint;
	}
	Masspoint getMasspoint1() {
		return this->masspoint1;
	}
	void setMasspoint2(Masspoint masspoint) {
		this->masspoint2 = masspoint;
	}
	Masspoint getMasspoint2() {
		return this->masspoint2;
	}
	void setMassIndex1(int index) {
		this->massIndex1 = index;
	}
	void setMassIndex2(int index) {
		this->massIndex2 = index;
	}
	int getMassIndex1() {
		return this->massIndex1;
	}
	int getMassIndex2() {
		return this->massIndex2;
	}
	void setInitialLength(float initialLength) {
		this->initialLength = initialLength;
	}
	float getInitialLength() {
		return this->initialLength;
	}
	void setStiffness(float stiffness) {
		this->stiffness = stiffness;
	}
	float getStiffness() {
		return this->stiffness;
	}

private:
	Masspoint masspoint1;
	Masspoint masspoint2;
	int massIndex1;
	int massIndex2;
	float initialLength;
	float stiffness;
};

class RigidBody {
public:
	RigidBody() {}

	RigidBody(Vec3 position, Vec3 size, int mass) {
		// Given Variables: Position, Size, Mass
		this->position = position;
		this->size = size;
		this->massInverse = 1.0f / mass;
		
		// Null Variables: Velocity, Angular Velocity, Orientation
		this->velocity = Vec3(0.0f, 0.0f, 0.0f);
		this->angularVelocity = Vec3(0.0f, 0.0f, 0.0f);
		this->orientation = Quat(1.0f, 1.0f, 1.0f, 1.0f);
		for (int i = 0; i < 8; i++) {
			this->vertexForces.push_back(Vec3());
		}
		this->forceAccumulator = Vec3(0.0f, 0.0f, 0.0f);
		this->torqueAccumulator = Vec3(0.0f, 0.0f, 0.0f);

		// Vertices
		this->vertices.push_back(Vec3(size.x / 2.0f, size.y / 2.0f, size.z / 2.0f));
		this->vertices.push_back(Vec3(size.x / 2.0f, size.y / 2.0f, -size.z / 2.0f));
		this->vertices.push_back(Vec3(size.x / 2.0f, -size.y / 2.0f, size.z / 2.0f));
		this->vertices.push_back(Vec3(size.x / 2.0f, -size.y / 2.0f, -size.z / 2.0f));
		this->vertices.push_back(Vec3(-size.x / 2.0f, size.y / 2.0f, size.z / 2.0f));
		this->vertices.push_back(Vec3(-size.x / 2.0f, size.y / 2.0f, -size.z / 2.0f));
		this->vertices.push_back(Vec3(-size.x / 2.0f, -size.y / 2.0f, size.z / 2.0f));
		this->vertices.push_back(Vec3(-size.x / 2.0f, -size.y / 2.0f, -size.z / 2.0f));

		// Inertia Tensor
		XMMATRIX C = XMMATRIX(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
		for (int i = 0; i < 8; i++) {
			C += (8 / this->massInverse) * XMMATRIX(
				(float)(vertices.at(i).y*vertices.at(i).y * vertices.at(i).z*vertices.at(i).z), (float)(-vertices.at(i).x * vertices.at(i).y), (float)(-vertices.at(i).x * vertices.at(i).z), 0.0f,
				(float)(-vertices.at(i).x * vertices.at(i).y), (float)(vertices.at(i).x*vertices.at(i).x*vertices.at(i).z* vertices.at(i).z), (float)(-vertices.at(i).y*vertices.at(i).z), 0.0f,
				(float)(-vertices.at(i).x * vertices.at(i).z), (float)(-vertices.at(i).y*vertices.at(i).z), (float)(vertices.at(i).x*vertices.at(i).x * vertices.at(i).y*vertices.at(i).y), 0.0f,
				0.0f, 0.0f, 0.0f, 1.0f);
		}
		XMMATRIX I = XMMATRIX(C.r[0].m128_f32[0]+ C.r[1].m128_f32[1]+ C.r[2].m128_f32[2], 0.0f, 0.0f, 0.0f,
			0.0f, C.r[0].m128_f32[0] + C.r[1].m128_f32[1] + C.r[2].m128_f32[2], 0.0f, 0.0f,
			0.0f, 0.0f, C.r[0].m128_f32[0] + C.r[1].m128_f32[1] + C.r[2].m128_f32[2], 0.0f,
			0.0f, 0.0f, 0.0f, 1.0f) - C;
		
		XMVECTOR length = XMVector4Length(XMMatrixDeterminant(I));
		if (length.m128_f32[0] > 0.0f) {
			this->inertiaTensorInverse = Mat4(XMMatrixInverse(nullptr, I));
		}
		else {
			this->inertiaTensorInverse = Mat4(1.0f, 0.0f, 0.0f, 0.0f,
												0.0f, 1.0f, 0.0f, 0.0f,
												0.0f, 0.0f, 1.0f, 0.0f,
												0.0f, 0.0f, 0.0f, 1.0f);
		}

	}
	
	Vec3 getSize() {
		return this->size;
	}
	void setSize(Vec3 size) {
		this->size = size;
	}
	float getMassInverse() {
		return this->massInverse;
	}
	void setMassInverse(float massInverse) {
		this->massInverse = massInverse;
	}
	Vec3 getPosition() {
		return this->position;
	}
	void setPosition(Vec3 position) {
		this->position = position;
	}
	Vec3 getVelocity() {
		return this->velocity;
	}
	void setVelocity(Vec3 velocity) {
		this->velocity = velocity;
	}
	Mat4 getInertiaTensorInverse() {
		return this->inertiaTensorInverse;
	}
	void setInertiaTensorInverse(Mat4 inertiaTensorInverse) {
		this->inertiaTensorInverse = inertiaTensorInverse;
	}
	Quat getOrientation() {
		return this->orientation;
	}
	void setOrientation(Quat orientation) {
		this->orientation = orientation;
	}
	Vec3 getAngularVelocity() {
		return this->angularVelocity;
	}
	void setAngularVelocity(Vec3 avc) {
		this->angularVelocity = avc;
	}
	Vec3 getForceAccumulator() {
		return this->forceAccumulator;
	}
	void setForceAccumulator(Vec3 forceAccumulator) {
		this->forceAccumulator = forceAccumulator;
	}
	Vec3 getTorqueAccumulator() {
		return this->torqueAccumulator;
	}
	void setTorqueAccumulator(Vec3 torqueAccumulator) {
		this->torqueAccumulator = torqueAccumulator;
	}
	Mat4 getTransform() {
		return this->transform;
	}
	void setTransform(Mat4 transform) {
		this->transform = transform;
	}

	Vec3 getAngularMomentum() {
		return this->angularMomentum;
	}
	void setAngularMomentum(Vec3 momentum) {
		this->angularMomentum = momentum;
	}

	std::vector<Vec3> vertices;
	std::vector<Vec3> vertexForces;


private:
	float massInverse;
	Vec3 size;
	Vec3 position;
	Vec3 velocity;
	Mat4 inertiaTensorInverse;
	Quat orientation;
	Vec3 angularVelocity;
	Vec3 angularMomentum;

	Vec3 forceAccumulator;
	Vec3 torqueAccumulator;
	Mat4 transform;
};