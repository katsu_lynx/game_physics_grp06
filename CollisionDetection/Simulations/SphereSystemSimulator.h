#ifndef SPHSYSTEMSIMULATOR_h
#define SPHSYSTEMSIMULATOR_h
#include "Simulator.h"
#include "Objects.h"
//#include "spheresystem.h", add your sphere system header file

#define NAIVEACC 0
#define GRIDACC 1

class SphereSystemSimulator:public Simulator{
public:
	// Construtors
	SphereSystemSimulator();
	// Functions
	const char * getTestCasesStr();
	void initUI(DrawingUtilitiesClass * DUC);
	void reset();
	void drawFrame(ID3D11DeviceContext* pd3dImmediateContext);
	void notifyCaseChanged(int testCase);
	void externalForcesCalculations(float timeElapsed);
	void simulateTimestep(float timeStep);
	void applyExternalForce(Vec3 force);
	double calculateDistance(Masspoint * masspoint1, Masspoint * masspoint2);
	void onClick(int x, int y);
	void onMouse(int x, int y);
	
protected:
	// Attributes
	Vec3 externalForce;
	Point2D m_mouse;
	Point2D m_trackmouse;
	Point2D m_oldtrackmouse;
	float m_fMass;
	float m_fRadius;
	float m_fForceScaling;
	float m_fDamping;

	//Collision scalling factor
	float m_lambda = 1;

	int   m_iNumSpheres;
	
	//Data Attributes
	float m_fGravity = 9.81;
	bool gravity = 0;
	int detectMode = 0; //0 is bruteforce, 1 is grid, 2 is KD-Tree

	//UI Attributes
	Vec3 m_externalForce = Vec3(0, 0, 0);
	
	int   m_iKernel; // index of the m_Kernels[5], more detials in SphereSystemSimulator.cpp
	static std::function<float(float)> m_Kernels[5];
	
	int   m_iAccelerator; // switch between NAIVEACC and GRIDACC, (optionally, KDTREEACC, 2)
	
	//SphereSystem * m_pSphereSystem; // add your own sphere system member!
	// for Demo 3 only:
	// you will need multiple SphereSystem objects to do comparisons in Demo 3
	// m_iAccelerator should be ignored.
	//SphereSystem * m_pSphereSystemGrid; 
	std::vector<Masspoint> masspoints;
};

#endif