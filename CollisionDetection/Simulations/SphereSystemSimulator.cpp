﻿#include "SphereSystemSimulator.h"

std::function<float(float)> SphereSystemSimulator::m_Kernels[5] = {
	[](float x) {return 1.0f; },              // Constant, m_iKernel = 0
	[](float x) {return 1.0f - x; },          // Linear, m_iKernel = 1, as given in the exercise Sheet, x = d/2r
	[](float x) {return (1.0f - x)*(1.0f - x); }, // Quadratic, m_iKernel = 2
	[](float x) {return 1.0f / (x)-1.0f; },     // Weak Electric Charge, m_iKernel = 3
	[](float x) {return 1.0f / (x*x) - 1.0f; },   // Electric Charge, m_iKernel = 4
};

// SphereSystemSimulator member functions

const char * SphereSystemSimulator::getTestCasesStr() {
	return " ";
};

void SphereSystemSimulator::initUI(DrawingUtilitiesClass * DUC) {
	this->DUC = DUC;
	switch (m_iTestCase) {
	case 0: break;
	case 1: break;
	case 2: break;
	default: break;
	}
};

void SphereSystemSimulator::reset() {
	m_mouse.x = m_mouse.y = 0;
	m_trackmouse.x = m_trackmouse.y = 0;
	m_oldtrackmouse.x = m_oldtrackmouse.y = 0;
	this->notifyCaseChanged(this->m_iTestCase);
};

void SphereSystemSimulator::drawFrame(ID3D11DeviceContext* pd3dImmediateContext) {
	std::mt19937 eng;
	std::uniform_real_distribution<float> randCol(0.0f, 1.0f);

	for (Masspoint masspoint : this->masspoints) {
		DUC->setUpLighting(Vec3(), 0.4*Vec3(1, 1, 1), 100, 0.6*Vec3(randCol(eng), randCol(eng), randCol(eng)));
		DUC->drawSphere(masspoint.getPosition(), Vec3(0.1f, 0.1f, 0.1f));
	}
};

void SphereSystemSimulator::notifyCaseChanged(int testCase) {
	this->m_iTestCase = testCase;
	this->masspoints.clear();
	switch (m_iTestCase)
	{
	case 0:
		break;
	case 1:
		break;
	case 2:
		break;
	default:
		break;
	}
};

void SphereSystemSimulator::externalForcesCalculations(float timeElapsed) {

};

void SphereSystemSimulator::simulateTimestep(float timeStep) {
	//Apply gravity
	if (this->gravity) {
		applyExternalForce(Vec3(0, this->m_fGravity, 0));
	}

	//Collision detection
	if (this->detectMode == 0) {
		//Brute force
		for (int i = 0; i < masspoints.size(); i++) {
			Masspoint * masspoint1 = &masspoints.at(i);
			for (int j = i+1; j < masspoints.size(); j++) {
				Masspoint * masspoint2 = &masspoints.at(j);
				double d = calculateDistance(masspoint1, masspoint2);
				if (d < 2 * this->m_fRadius) {
					//Colliding objects Masspoint1 and masspoint2
					float f = this->m_lambda * (1-d/(2*this->m_fRadius));
					masspoint1->setAcceleration(masspoint1->getAcceleration() + f / masspoint1->getMass());
					masspoint2->setAcceleration(masspoint2->getAcceleration() + f / masspoint2->getMass());
				}
			}
		}
	} else if (this->detectMode == 1) {
		//Grid

	} else if (this->detectMode == 1) {
		//KD-Tree
		//No time to implement

	}

	//Velocity and acceleration computations
	for (int i = 0; i < masspoints.size(); i++) {
		Masspoint * masspoint = &masspoints.at(i);
		if (!masspoint->getIsFixed()) {
			masspoint->setAcceleration((-masspoint->getAcceleration() + this->m_externalForce) / masspoint->getMass());
			masspoint->setVelocity(masspoint->getVelocity() + timeStep * masspoint->getAcceleration());
			masspoint->setPosition(masspoint->getPosition() + timeStep * masspoint->getVelocity());
		}
		else {
			masspoint->setVelocity(Vec3(0, 0, 0));
		}
		masspoint->setAcceleration(Vec3(0, 0, 0));
	}

	

};

double SphereSystemSimulator::calculateDistance(Masspoint * masspoint1, Masspoint * masspoint2) {
	Vec3 vector = masspoint1->getPosition() - masspoint2->getPosition();
	return sqrt(vector.x * vector.x + vector.y * vector.y + vector.z * vector.z);
};

void SphereSystemSimulator::applyExternalForce(Vec3 force) {
	this->m_externalForce += force;
}

void SphereSystemSimulator::onClick(int x, int y) {
	m_trackmouse.x += x - m_oldtrackmouse.x;
	m_trackmouse.y += y - m_oldtrackmouse.y;
};

void SphereSystemSimulator::onMouse(int x, int y) {
	m_oldtrackmouse.x = x;
	m_oldtrackmouse.y = y;
};