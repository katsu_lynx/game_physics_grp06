#include "pingpongsystem.h"
#include "DrawingUtilitiesClass.h"
#include <algorithm>


using namespace GamePhysics;
PingPongSystem::~PingPongSystem()
{
}
void PingPongSystem::SceneSetup(int sceneflag)
{
	m_points.clear();
	m_springs.clear();
	m_rigidBodies.clear();
	m_paddles.clear();


	this->spawnBall();
	m_score = 0;

	switch (sceneflag)
	{
	case 0:
	{

		/*int p1 = AddPoint(Vec3(0, -1.1, 2), true);
		int p2 = AddPoint(Vec3(0, 0, 2), false);
		AddSpring(p1, p2);*/
		
		int countBalls = 20;
		for (int i = 0; i < countBalls; i++) {

			//Polarkoordinaten, r =radius, w = winkel
			float r = (rand() / (float)RAND_MAX) * 3.3 + 0.5;
			float w = (rand() / (float)RAND_MAX) * 2 * M_PI;

			//Umwandeln in Kartesische Koordinaten
			float x = r*cos(w);
			float y = r*sin(w);
			int p1 = AddPoint(Vec3(x, -1.1, y), true);
			int p2 = AddPoint(Vec3(x, 0, y), false);
			AddSpring(p1, p2);
		}
		
		//m_gravity = Vec3(0, -9.81f, 0);
		/*
		int p1 = AddPoint(Vec3(0.25, 0.5, 0), true);
		int p2 = AddPoint(Vec3(0.25, 0.2, 0), false);
		int p3 = AddPoint(Vec3(0.45, 0.2, 0), false);
		AddSpring(p1, p2);
		AddSpring(p2, p3);

		int p4 = AddPoint(Vec3(-0.25f, 0.5f, 0), true);
		int p5 = AddPoint(Vec3(-0.25f, 0.2f, 0), false);
		AddSpring(p4, p5);

		{
			static const float  t = 1.618033988749894848205f; // (1 + sqrt(5)) / 2
			static const float t2 = 1.519544995837552493271f; // sqrt( 1 + sqr( (1 + sqrt(5)) / 2 ) )

			static const Vec3 verts[13] = {
				Vec3(t / t2, 1.f / t2, 0),
				Vec3(-t / t2, 1.f / t2, 0),
				Vec3(t / t2, -1.f / t2, 0),
				Vec3(-t / t2, -1.f / t2, 0),
				Vec3(1.f / t2, 0, t / t2),
				Vec3(1.f / t2, 0, -t / t2),
				Vec3(-1.f / t2, 0, t / t2),
				Vec3(-1.f / t2, 0, -t / t2),
				Vec3(0, t / t2, 1.f / t2),
				Vec3(0, -t / t2, 1.f / t2),
				Vec3(0, t / t2, -1.f / t2),
				Vec3(0, -t / t2, -1.f / t2),
				Vec3(0, 0, 0)
			};

			static const int faces[20 * 3] =
			{
				0, 8, 4,
				0, 5, 10,
				2, 4, 9,
				2, 11, 5,
				1, 6, 8,
				1, 10, 7,
				3, 9, 6,
				3, 7, 11,
				0, 10, 8,
				1, 8, 10,
				2, 9, 11,
				3, 11, 9,
				4, 2, 0,
				5, 0, 2,
				6, 1, 3,
				7, 3, 1,
				8, 6, 4,
				9, 4, 6,
				10, 5, 7,
				11, 7, 5
			};

			float size = 0.1f;

			int pi[13];
			for (int i = 0; i < 13; i++)
			{
				Vec3 pos = verts[i] * size;
				pi[i] = AddPoint(pos, false);
			}

			for (int i = 0; i < 12; i++)
			{
				AddSpring(pi[i], pi[12]);
			}

			for (int i = 0; i < 20; i++)
			{
				if (faces[i * 3 + 0] < faces[i * 3 + 1]) AddSpring(pi[faces[i * 3 + 0]], pi[faces[i * 3 + 1]]);
				if (faces[i * 3 + 1] < faces[i * 3 + 2]) AddSpring(pi[faces[i * 3 + 1]], pi[faces[i * 3 + 2]]);
				if (faces[i * 3 + 2] < faces[i * 3 + 0]) AddSpring(pi[faces[i * 3 + 2]], pi[faces[i * 3 + 0]]);
			}

			//int p6 = AddPoint(XMFLOAT3(0,0.5,0), true);
			//AddSpring(pi[0], p6, 0.25f);
		}
		
		if (m_cube > 1)
		{
			std::vector<int> pi;

			for (int z = 0; z < m_cube; z++)
			for (int y = 0; y < m_cube; y++)
			for (int x = 0; x < m_cube; x++)
			{
				float width = 0.4f;
				Vec3 pos(-width / 2 + (float)x / (m_cube - 1) * width,
					-0.5f + (float)y / (m_cube - 1) * width,
					-width / 2 + (float)z / (m_cube - 1) * width);
				pi.push_back(AddPoint(pos, false));
			}

			for (int z = 0; z < m_cube; z++)
			for (int y = 0; y < m_cube; y++)
			for (int x = 0; x < m_cube; x++)
			{
				if (x < m_cube - 1) AddSpring(pi[(z * m_cube + y) * m_cube + x], pi[(z * m_cube + y) * m_cube + (x + 1)]);
				if (y < m_cube - 1) AddSpring(pi[(z * m_cube + y) * m_cube + x], pi[(z * m_cube + (y + 1)) * m_cube + x]);
				if (z < m_cube - 1) AddSpring(pi[(z * m_cube + y) * m_cube + x], pi[((z + 1) * m_cube + y) * m_cube + x]);

				if (x < m_cube - 1 && y < m_cube - 1) AddSpring(pi[(z * m_cube + y) * m_cube + x], pi[(z * m_cube + (y + 1)) * m_cube + (x + 1)]);
				if (y < m_cube - 1 && z < m_cube - 1) AddSpring(pi[(z * m_cube + y) * m_cube + x], pi[((z + 1) * m_cube + (y + 1)) * m_cube + x]);
				if (x < m_cube - 1 && z < m_cube - 1) AddSpring(pi[(z * m_cube + y) * m_cube + x], pi[((z + 1) * m_cube + y) * m_cube + (x + 1)]);

				if (x < m_cube - 1 && y < m_cube - 1 && z < m_cube - 1) AddSpring(pi[(z * m_cube + y) * m_cube + x], pi[((z + 1) * m_cube + (y + 1)) * m_cube + (x + 1)]);
			}
		}
		
		
		//rigid bodies
		m_rigidBodies.emplace_back(Vec3(-0.1f, -0.2f, 0.1f), Vec3(0.4f, 0.2f, 0.2f), 100.0f);
		m_rigidBodies.back().update(0.0f);
		m_rigidBodies.emplace_back(Vec3(0.0f, 0.2f, 0.0f), Vec3(0.4f, 0.2f, 0.2f), 100.0f);
		// Quat normal axis not used here
		m_rigidBodies.back().setRotation(Quat(Vec3(0.0f, 0.0f, 1.0f), (float)(M_PI)*0.25f));
		m_rigidBodies.back().setVelocity(Vec3(0.0f, 0.0f, 10.0f));
		m_rigidBodies.back().update(0.0f);
		
		*/
		//paddles

		m_paddles.emplace_back(4.0f, (float)(M_PI)*0.0f, Vec3(0.1f, 1.0f, 4.0f));
		m_paddles.emplace_back(4.0f, (float)(M_PI)*1.0f, Vec3(0.1f, 1.0f, 4.0f));
		m_paddles.emplace_back(4.0f, (float)(M_PI)*0.5f, Vec3(0.1f, 1.0f, 4.0f));
		m_paddles.emplace_back(4.0f, (float)(M_PI)*1.5f, Vec3(0.1f, 1.0f, 4.0f));
		//AddPaddle(4.0f, (float)(M_PI)*0.5f, Vec3(0.1f, 1.0f, 1.0f));
		//AddPaddle(4.0f, (float)(M_PI)*1.00f, Vec3(0.1f, 1.0f, 1.0f));


	}
	break;
	default:
		break;
	}
}


void PingPongSystem::spawnBall() {
	this->m_ball.pos = Vec3(0, 0, 0);

	Vec3 positions[9] = { Vec3(sqrt(2),0,0), Vec3(1,0,1), Vec3(0,0,sqrt(2)), Vec3(-sqrt(2),0,0), Vec3(-1,0,1), Vec3(-1,0,-1), Vec3(0,0,-sqrt(2)), Vec3(1,0,-1), Vec3(-1,0,-1) };
	int RandIndex = rand() % 9;
	this->m_ball.vel = positions[0] * 10;
	this->m_ball.radius = (0.2f, 0.2f, 0.2f);
}


void PingPongSystem::AddRigidBody(Vec3 position, Vec3 size, int mass)
{
	m_rigidBodies.emplace_back(position, size, mass);
	m_rigidBodies.back().update(0.0f);

}

/*
void PingPongSystem::AddPaddle(float radius, float angle, Vec3 size)
{
	Paddle paddle;
	paddle.m_radius = radius;
	paddle.m_angle = angle;
	paddle.m_height = 0.0f;
	paddle.m_center = Vec3(radius*cos(angle), paddle.m_height, radius*sin(angle));
	paddle.m_rotation = Quat(Vec3(0.0f, 1.0f, 0.0f), -angle);
	paddle.m_scale=size;

	GamePhysics::Mat4 matrix;
	matrix.initTranslation(paddle.m_center.x, paddle.m_center.y, paddle.m_center.z);
	paddle.m_scaledObjToWorld = paddle.m_rotation.getRotMat() * matrix;
	paddle.m_worldToScaledObj = paddle.m_scaledObjToWorld.inverse();
	matrix.initScaling(paddle.m_scale.x, paddle.m_scale.y, paddle.m_scale.z);
	paddle.m_objToWorld = matrix * paddle.m_scaledObjToWorld;
	paddle.m_worldToObj = paddle.m_objToWorld.inverse();

	m_paddles.push_back(paddle);
}
*/

void PingPongSystem::AdvanceEuler(float dt)
{
	std::vector<Vec3> forces = ComputeForces(dt);

	UpdatePositions(dt);

	UpdateVelocities(dt, forces);
}

void PingPongSystem::AdvanceLeapFrog(float dt)
{
	std::vector<Vec3> forces = ComputeForces(dt);
	
	UpdateVelocities(dt, forces);
	
	UpdatePositions(dt);
}

void PingPongSystem::AdvanceMidPoint(float dt)
{
	std::vector<Point> ori_points = m_points;// save x_old, v_old

	std::vector<Vec3> forces = ComputeForces(dt);// force = a( x_old, v_old), force_old
	UpdatePositions(dt / 2.0f); // x = x_tmp, using v_old
	UpdateVelocities(dt / 2.0f, forces); // v = v_tmp, using force_old

	forces = ComputeForces(dt);// force = a ( x_tmp, v_tmp )

	for (size_t i = 0; i < m_points.size(); i++)//restore x_old
	{
		m_points[i].pos = ori_points[i].pos;
	}
	UpdatePositions(dt);// x = x ( vtmp )
	for (size_t i = 0; i < m_points.size(); i++)//restore v_old
	{
		m_points[i].vel = ori_points[i].vel;
	}	
	UpdateVelocities(dt, forces);// v = v( a (xtmp, vtmp) )
}


std::vector<Vec3> PingPongSystem::ComputeForces(float dt)
{
	// Gravity forces

	std::vector<Vec3> forces(m_points.size(), m_gravity * m_mass);

	for (size_t i=0; i<m_springs.size(); i++)
	{
		// Spring forces
		int p1 = m_springs[i].point1;
		int p2 = m_springs[i].point2;
		Vec3 pos1 = m_points[p1].pos;
		Vec3 pos2 = m_points[p2].pos;
		
		Vec3 d = pos1 - pos2;
		float l = norm(d);
		float L = m_springs[i].initialLength;
		float k = m_stiffness;

		Vec3 f = d * (- k * (l - L) / l);

		forces[p1] += f;
		forces[p2] -= f;
	}

	// Damping forces
	for (size_t i=0; i<m_points.size(); i++)
	{
		Vec3 vel = m_points[i].vel;
	
		forces[i] += vel * -m_damping;
	}

	//Add m_Ball colision force
	Vec3 ballPos = this->m_ball.pos;
	for (int i = 0; i < this->m_points.size(); i++) {
		Point p = this->m_points.at(i);
		Vec3 c = ballPos - p.pos;
		float distance = sqrt(c.x*c.x + c.y*c.y + c.z*c.z);
				
		Vec3 posi = m_points[i].pos;
		Vec3 posj = m_ball.pos;

		Vec3 direction = posi - posj;
		double d = sqrt(direction.x * direction.x + direction.y * direction.y + direction.z * direction.z);

		if (distance <= 0.3) {
			Vec3 f = (1 - d / (2 * 0.3));
			float m = m_mass;
			m_points[i].vel += direction * f * (dt / m);
		}
	}
	return forces;
}

void PingPongSystem::UpdatePositions(float dt)
{
	this->m_ball.pos = this->m_ball.pos + dt*this->m_ball.vel;

	for (size_t i=0; i<m_points.size(); i++)
	{
		if (!m_points[i].fixed)
		{
			Vec3 pos = m_points[i].pos;
			Vec3 vel = m_points[i].vel;

			pos +=  vel * dt;
			m_points[i].pos =  pos;
		}
	}
}

void PingPongSystem::UpdateVelocities(float dt, const std::vector<Vec3>& forces)
{
	//this->m_ball.vel = this->m_ball.vel + m_gravity * m_mass * (dt / m_mass);
	for (size_t i=0; i<m_points.size(); i++)
	{
		if (!m_points[i].fixed)
		{
			Vec3 vel = m_points[i].vel;
			float m = m_mass;

			vel +=  forces[i] * (dt / m);

			m_points[i].vel =  vel;
		}
		else
		{
			m_points[i].vel = Vec3(0,0,0);
		}
	}
}

void PingPongSystem::BoundingBoxCheck(float times)
{
	for (size_t i = 0; i < m_points.size(); i++)
	{
		if (!m_points[i].fixed)
		{
			Vec3 pos = m_points[i].pos;
			Vec3 vel = m_points[i].vel;

			if (m_points[i].pos.y <= -1.0 + m_points[i].radius.x) {
				m_points[i].vel = -m_points[i].vel;
			}



			/*for (int f = 0; f < 6; f++)
			{
				float sign = (f % 2 == 0) ? -1.0f : 1.0f;
				if (sign * pos.value[f / 2] < -0.5f * times)
				{
					pos.value[f / 2] = sign * -0.5f * times;
					vel.value[f / 2] =  0;
				}
			}

			m_points[i].pos =  pos;
			m_points[i].vel = vel;*/
		}
	}
	if (m_ball.pos.y <= -1.0 + m_ball.radius.x) {
		m_ball.vel.y = -m_ball.vel.y;
	}
}

void PingPongSystem::dragTogether()
{
	for (int i = 0; i < m_rigidBodies.size() - 1; ++i)
	{
		Vec3 vel = m_rigidBodies[i + 1].getCenter() - m_rigidBodies[i].getCenter();
		m_rigidBodies[i].setVelocity(vel * 0.1f);
		m_rigidBodies[i + 1].setVelocity(vel * -0.1f);
	}
}

void PingPongSystem::addGlobalFrameForce(const Vec3 force)
{
	for (RigidBody& rigidBody : m_rigidBodies)
	{
		rigidBody.addForceWorld(force, XMVectorZero());
	}
}

double PingPongSystem::calculateDistance(Vec3 vector) {
	return sqrt(vector.x * vector.x + vector.y * vector.y + vector.z * vector.z);
};

void PingPongSystem::updateCollisions(float dt) {

	Vec3 ballPos = this->m_ball.pos;
	Vec3 ballRadius = this->m_ball.radius;
	float m_lambda = 100;
	Vec3 direction = (0.0f, 0.0f, 0.0f);
	double distance = 0.0f;
	double radiusdistance = 0.0f;

	//points
	for (size_t i = 0; i < m_points.size(); i++)
	{
		//points
		Vec3 posi = m_points[i].pos;
		for (size_t j = i + 1; j < m_points.size(); j++)
		{
			Vec3 posj = m_points[j].pos;
			direction = posi - posj;
			distance = calculateDistance(direction);
			direction = direction / distance;
			radiusdistance = m_points[j].radius.x + m_points[i].radius.x;
			if (distance < radiusdistance) {
				Vec3 f = m_lambda * (1 - distance / radiusdistance);
				float m = m_mass;
				m_points[i].vel += direction * f * (dt / m);
				m_points[j].vel += -direction * f * (dt / m);
			}
		}
		//ball
		direction = posi - ballPos;
		distance = calculateDistance(direction);
		direction = direction / distance;
		radiusdistance = ballRadius.x + m_points[i].radius.x;
		if (distance < radiusdistance) {
			Vec3 ballVel = this->m_ball.vel;
			Vec3 f = m_lambda * (1 - distance / radiusdistance);
			float m = m_mass;
			m_points[i].vel += direction * f * (dt / m);
			//Change color
			if (!this->m_points.at(i).striked) {
				this->m_points.at(i).striked = true;
				m_score++;
			}
		}

		/*/paddles
		float blength = calculateDistance(posi);
		for (int i = 0; i < m_paddles.size(); i++) {

			Paddle p = this->m_paddles.at(i);
			float radiuslimit = p.m_radius - p.m_scale.x / 2 - m_points[i].radius.x;
			float paddlelimit = p.m_scale.z / 2 + m_points[i].radius.z;
			if (blength >= radiuslimit) {


				//direction vector paddle
				Vec3 t;
				t.x = -p.m_center.z;
				t.y = 0;
				t.z = p.m_center.x;

				float tlength = sqrt(t.x*t.x + t.z*t.z);
				t.x = t.x / tlength;
				t.z = t.z / tlength;
				bool isColliding = false;

				//sphere position
				Vec3 b(posi.x / blength, 0, posi.z / blength);

				float lambda = (b.x*p.m_center.z - b.z*p.m_center.x) / (b.z*t.x - b.x*t.z);
				if (abs(lambda) <= paddlelimit) {
					//intersection paddle and ball line
					Vec3 s = p.m_center + lambda*t;
					float slength = sqrt(s.x*s.x + s.z*s.z);
					//signvector
					Vec3 vs(s.x > 0.0f, s.y > 0.0f, s.z > 0.0f);
					Vec3 vb(b.x > 0.0f, b.y > 0.0f, b.z > 0.0f);
					if (slength - 0.25 <= blength&&blength <= slength - 0.15&&vs.toString() == vb.toString())isColliding = true;

				}
				if (isColliding) {
					Vec3 ballVel = this->m_points[i].vel;
					float phi = acos((t.x*ballVel.x + t.z*ballVel.z) / (tlength*blength));
					this->rotatePointY(this->m_points[i], -2 * phi);
					this->m_points[i].pos = this->m_points[i].pos + dt*this->m_ball.vel;
				}
			}
		}*/
	}

	//ball
	/*
	for (int i = 0; i < this->m_points.size(); i++) {
		Point p = this->m_points.at(i);
		Vec3 c = ballPos - p.pos;
		float distance = sqrt(c.x*c.x + c.y*c.y + c.z*c.z);

		if (distance <= 0.3) {
			// Ball velocity Minus Point position
			Vec3 bMp = this->m_ball.vel - p.pos;
			// Ball velocity Minus Point position Length
			float bMpL = sqrt(bMp.x*bMp.x + bMp.y*bMp.y + bMp.z*bMp.z);
			// Ball velocity Length Squared
			float bLS = (this->m_ball.vel.x*this->m_ball.vel.x + this->m_ball.vel.z*this->m_ball.vel.z);
			// Point position Length Squared
			float pLS = (p.pos.x*p.pos.x + p.pos.y*p.pos.y + p.pos.z*p.pos.z);
			// Rotation Vector phi
			float phi = acos(std::min(1.0f, std::max(-1.0f,-(bMpL - bLS - pLS)/(2*bLS*pLS))));

			// Rotate velocity vector by phi
			this->rotateBall(M_PI-phi);

			//Change color
			this->m_points.at(i).striked = true;

			//TODO:Add a score
			m_score++;
		}
	}
	*/

	//ball-paddle collision
	float blength = sqrt(ballPos.x*ballPos.x + ballPos.z*ballPos.z);

	for (int i = 0; i < m_paddles.size(); i++) {

		Paddle p = this->m_paddles.at(i);
		float radiuslimit = p.m_radius - p.m_scale.x / 2 - ballRadius.x;
		float paddlelimit = p.m_scale.z / 2 + ballRadius.z;
		if (blength >= radiuslimit) {


			//direction vector paddle
			Vec3 t;
			t.x = -p.m_center.z;
			t.y = 0;
			t.z = p.m_center.x;

			float tlength = sqrt(t.x*t.x + t.z*t.z);
			t.x = t.x / tlength;
			t.z = t.z / tlength;

			bool isColliding = false;

			//ball position
			Vec3 b(ballPos.x / blength, 0, ballPos.z / blength);

			float lambda = (b.x*p.m_center.z - b.z*p.m_center.x) / (b.z*t.x - b.x*t.z);
			//float m�h = abs((p.m_center.x + lambda*t.x) / b.x);


			if (abs(lambda) <= paddlelimit) {
				//intersection paddle and ball line
				Vec3 s = p.m_center + lambda*t;
				float slength = sqrt(s.x*s.x + s.z*s.z);
				//signvector
				Vec3 vs(s.x > 0.0f, s.y > 0.0f, s.z > 0.0f);
				Vec3 vb(b.x > 0.0f, b.y > 0.0f, b.z > 0.0f);
				if (slength - 0.25 <= blength&&blength <= slength - 0.15&&vs.toString() == vb.toString())isColliding = true;

			}



			if (isColliding) {
				Vec3 ballVel = this->m_ball.vel;
				float phi = acos((t.x*ballVel.x + t.z*ballVel.z) / (tlength*blength));
				this->rotatePointY(this->m_ball, -2 * phi);
				this->m_ball.pos = this->m_ball.pos + dt*this->m_ball.vel;
				//this->m_ball.pos = this->m_ball.pos + 0.01*-p.m_center;
				//m_gravity=(-1)*m_gravity;

				/*Vec3 n = Vec3(p.m_radius*cos(p.m_angle), 0, p.m_radius*sin(p.m_angle));

				Vec3 bMp = this->m_ball.vel - n;
				float bMpL = sqrt(bMp.x*bMp.x + bMp.y*bMp.y + bMp.z*bMp.z);
				float bLS = (this->m_ball.vel.x*this->m_ball.vel.x + this->m_ball.vel.z*this->m_ball.vel.z);
				float pLS = (n.x*n.x + n.y*n.y + n.z*n.z);
				float phi = acos(-(bMpL - bLS - pLS) / (2 * bLS*pLS));
				this->rotateBall(phi + M_PI);
				*/
			}
		}
	}

	if (blength >= 4.5) {
		spawnBall();
		m_score--;
	}


		/*if (ballPos.x != 0.0f && ballPos.z != 0) {
	=======

		if (ballPos.x == 0.0f && ballPos.z == 0) {
	>>>>>>> 557a6e80c5f7d05a05ccef2faa6b1ff0fd3ec27a
			for (int i = 0; i < this->m_paddles.size(); i++) {
				Paddle p = this->m_paddles.at(i);



				// Ball to 'Paddle' conversion (useful?)
				//float radius = sqrt(ballPos.x*ballPos.x + ballPos.z*ballPos.z);
				//Paddle b = Paddle(radius, acos(ballPos.x / radius), Vec3(.2, .2, .2));

				bool isCollision = false;


				if (isCollision) {
					Vec3 n = Vec3(p.m_radius*cos(p.m_angle),0,p.m_radius*sin(p.m_angle));

					Vec3 bMp = this->m_ball.vel - n;
					float bMpL = sqrt(bMp.x*bMp.x + bMp.y*bMp.y + bMp.z*bMp.z);
					float bLS = (this->m_ball.vel.x*this->m_ball.vel.x + this->m_ball.vel.z*this->m_ball.vel.z);
					float pLS = (n.x*n.x + n.y*n.y + n.z*n.z);
					float phi = acos(-(bMpL - bLS - pLS) / (2 * bLS*pLS));
					this->rotateBall(phi + M_PI);
				}
			}
		}
		*/
	
}

void PingPongSystem::update(float deltaTime)
{

	for (RigidBody& rigidBody : m_rigidBodies)
	{
		rigidBody.update(deltaTime);
	}
	for (size_t i = 0; i < m_rigidBodies.size(); ++i)
	{	
		/*
		for (size_t j = 0; j < m_paddles.size(); j++) {

			Paddle p = this->m_paddles.at(j);
			if (RigidBody::collide(m_rigidBodies[i], p))
			{

			}
		}
		*/
		for (size_t j = i + 1; j < m_rigidBodies.size(); ++j)
		{
			

			if (RigidBody::collide(m_rigidBodies[i], m_rigidBodies[j]))
			{
			}
		}
	}
}

void PingPongSystem::handleKeys()
{
	if (DXUTIsKeyDown(VK_LEFT) || DXUTIsKeyDown('A')){
		for (int i = 0; i < m_paddles.size(); i++) {
			m_paddles[i].movePaddle(m_paddles[i].getRadius(), m_paddles[i].getAngle() - 0.01f, m_paddles[i].getHeight());
		}
	}
	if (DXUTIsKeyDown(VK_RIGHT) || DXUTIsKeyDown('D')) {
		for (int i = 0; i < m_paddles.size(); i++) {
			m_paddles[i].movePaddle(m_paddles[i].getRadius(), m_paddles[i].getAngle() + 0.01f, m_paddles[i].getHeight());
		}
	}
	/*if (DXUTIsKeyDown(VK_UP)) {
		h = h + 0.01f;
		m_paddles[0].movePaddle(r, a, h);
		m_paddles[1].movePaddle(r, a, h);
	}
	if (DXUTIsKeyDown(VK_DOWN)) {
		h = h - 0.01f;
		m_paddles[0].movePaddle(r, a, h);
		m_paddles[1].movePaddle(r, a, h);
	}*/

		
}