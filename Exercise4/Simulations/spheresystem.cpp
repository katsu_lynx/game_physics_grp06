#include "spheresystem.h"
#include "SphereSystemSimulator.h"
#include <map>
void SphereSystem::SceneSetup(int sceneflag)
{
	m_points.clear();
	m_lambda = 100;

	switch (sceneflag)
	{
	case 0:
	{
		std::mt19937 eng;
		std::uniform_real_distribution<float> randPos(-0.5f, 0.5f);
		std::uniform_real_distribution<float> randVel(-1.0f, 1.0f);
		m_gravity = Vec3(0, 0, 0);
		m_mass = 10.0f;
		m_damping = 0.0f;

		for (int i = 0; i < m_number; i++) {
			float x = randPos(eng);
			float y = randPos(eng);
			float z = randPos(eng);

			int p0 = AddPoint(Vec3(x, y, z), false);

			x = randPos(eng);
			y = randPos(eng);
			z = randPos(eng);
			SetPointVelocity(p0, Vec3(x, y, z));
		}
		/*m_gravity = Vec3(0, 0, 0);
		m_mass = 10.0f;
		m_damping = 0.0f;

		for (int i = 0; i < m_number; i++) {
			int p0 = AddPoint(Vec3(0.0, -0.25f, 0), false);
			SetPointVelocity(p0, Vec3(0.0, 1.0f, 0.0));
		}

		int p0 = AddPoint(Vec3(0.0, -0.25f, 0), false);
		int p1 = AddPoint(Vec3(0.0, 0.25f, 0), false);
		SetPointVelocity(p0, Vec3(0.0, 1.0f, 0.0));
		SetPointVelocity(p1, Vec3(0.0, -1.0f, 0.0));
		*/
	}
	break;
	case 1:
	{
		m_gravity = Vec3(0, 0, 0);
		m_mass = 10.0f;
		m_damping = 0.0f;

		int p0 = AddPoint(Vec3(0.0, -0.25f, 0), false);
		int p1 = AddPoint(Vec3(0.0, 0.25f, 0), false);
		SetPointVelocity(p0, Vec3(0.0, 1.0f, 0.0));
		SetPointVelocity(p1, Vec3(0.0, -1.0f, 0.0));
	}
	break;
	case 2:
	{

		//Create 100 Balls randomly and add them to the sheresystem
		std::mt19937 eng;
		std::uniform_real_distribution<float> randPos(-0.5f, 0.5f);
		std::uniform_real_distribution<float> randVel(-1.0f, 1.0f);
		m_gravity = Vec3(0, 0, 0);
		m_mass = 10.0f;
		m_damping = 0.0f;

		for (int i = 0; i < 100; i++) {
			float x = randPos(eng);
			float y = randPos(eng);
			float z = randPos(eng);

			int p0 = AddPoint(Vec3(x, y, z), false);

			x = randPos(eng);
			y = randPos(eng);
			z = randPos(eng);
			SetPointVelocity(p0, Vec3(x, y, z));
		}


	}
	break;
	default:
		break;
	}
}




void SphereSystem::AdvanceEuler(float dt)
{
	std::vector<Vec3> forces = ComputeForces();

	UpdatePositions(dt);

	UpdateVelocities(dt, forces);
}

void SphereSystem::AdvanceLeapFrog(float dt)
{
	std::vector<Vec3> forces = ComputeForces();

	UpdateVelocities(dt, forces);

	UpdatePositions(dt);
}

void SphereSystem::AdvanceMidPoint(float dt)
{
	std::vector<Point> ori_points = m_points;// save x_old, v_old

	std::vector<Vec3> forces = ComputeForces();// force = a( x_old, v_old), force_old
	UpdatePositions(dt / 2.0f); // x = x_tmp, using v_old
	UpdateVelocities(dt / 2.0f, forces); // v = v_tmp, using force_old

	forces = ComputeForces();// force = a ( x_tmp, v_tmp )

	for (size_t i = 0; i < m_points.size(); i++)//restore x_old
	{
		m_points[i].pos = ori_points[i].pos;
	}
	UpdatePositions(dt);// x = x ( vtmp )
	for (size_t i = 0; i < m_points.size(); i++)//restore v_old
	{
		m_points[i].vel = ori_points[i].vel;
	}
	UpdateVelocities(dt, forces);// v = v( a (xtmp, vtmp) )
}

std::vector<Vec3> SphereSystem::ComputeForces()
{
	// Gravity forces

	std::vector<Vec3> forces(m_points.size(), m_gravity * m_mass);

	// Damping forces
	for (size_t i = 0; i<m_points.size(); i++)
	{
		Vec3 vel = m_points[i].vel;

		forces[i] += vel * -m_damping;
	}
	return forces;
}

void SphereSystem::UpdatePositions(float dt)
{
	for (size_t i = 0; i<m_points.size(); i++)
	{
		if (!m_points[i].fixed)
		{
			Vec3 pos = m_points[i].pos;
			Vec3 vel = m_points[i].vel;

			pos += vel * dt;
			m_points[i].pos = pos;
		}
	}
}

void SphereSystem::UpdateVelocities(float dt, const std::vector<Vec3>& forces)
{
	for (size_t i = 0; i<m_points.size(); i++)
	{
		if (!m_points[i].fixed)
		{
			Vec3 vel = m_points[i].vel;
			float m = m_mass;

			vel += forces[i] * (dt / m);

			m_points[i].vel = vel;
		}
		else
		{
			m_points[i].vel = Vec3(0, 0, 0);
		}
	}
}


void SphereSystem::BoundingBoxCheck(float times)
{
	for (size_t i = 0; i < m_points.size(); i++)
	{
		if (!m_points[i].fixed)
		{
			Vec3 pos = m_points[i].pos;
			Vec3 vel = m_points[i].vel;

			for (int f = 0; f < 6; f++)
			{
				float sign = (f % 2 == 0) ? -1.0f : 1.0f;
				if (sign * pos.value[f / 2] < -0.5f * times)
				{
					pos.value[f / 2] = sign * -0.5f * times;
					vel.value[f / 2] = -vel.value[f / 2];
				}
			}

			m_points[i].pos = pos;
			m_points[i].vel = vel;
		}
	}
}

void SphereSystem::CollisionCheck(int i, int j, float dt) {

	Vec3 posi = m_points[i].pos;
	Vec3 posj = m_points[j].pos;


	Vec3 direction = posi - posj;

	double d = calculateDistance(m_points[i], m_points[j]);
	if (d < 2 * m_radius) {
		Vec3 f = m_lambda * (1 - d / (2 * m_radius));
		float m = m_mass;
		m_points[i].vel += direction * f * (dt / m);
		m_points[j].vel += -direction * f * (dt / m);
	}

};

void SphereSystem::BruteForce(float dt) {

	//compute collisions
	for (size_t i = 0; i<m_points.size(); i++)
	{
		for (size_t j = i+1; j<m_points.size(); j++)
		{
			CollisionCheck(i, j, dt);
		}
	}
};

double SphereSystem::calculateDistance(Point masspoint1, Point masspoint2) {
	Vec3 vector = masspoint1.pos - masspoint2.pos;
	return sqrt(vector.x * vector.x + vector.y * vector.y + vector.z * vector.z);
};



void SphereSystem::Grid(float dt) {
	int CubesPerRow = (int)ceil(10.0f / (2 * m_radius));

	// Build & Populate Grid
	std::map<int, std::vector<int>> Grid = std::map<int, std::vector<int>>();
	for (int i = 0; i < m_points.size(); i++) {
		Point point = m_points.at(i);
		int cell_index = point.pos.x + point.pos.y * CubesPerRow + point.pos.z * CubesPerRow * CubesPerRow;
		if (Grid.find(cell_index) == Grid.end()) {
			std::vector<int> newV;
			newV.push_back(i);
			Grid[cell_index] = newV;
		}
		else {
			Grid[cell_index].push_back(i);
		}
	}

	typedef std::map<int, std::vector<int>>::iterator it_type;
	for (it_type iterator = Grid.begin(); iterator != Grid.end(); iterator++) {
		int key = iterator->first;
		std::vector<int> points = iterator->second;

		int z = floor(key / (CubesPerRow * CubesPerRow));
		int a = key - z * (CubesPerRow * CubesPerRow);
		int y = floor(a / CubesPerRow);
		int x = a - y * CubesPerRow;

		for (int i = -1; i < 2; i++) {
			for (int j = -1; j < 2; j++) {
				for (int k = -1; k < 2; k++) {
					if(i || j || k) {
						int cell_index_2 = i + x + (j + y)*CubesPerRow + (k + z)*CubesPerRow*CubesPerRow;
						if (Grid.find(cell_index_2) != Grid.end()) {
							std::vector<int> addPoints = Grid[cell_index_2];
							points.insert(points.end(), addPoints.begin(), addPoints.end());
						}
					}
				}
			}
		}

		for (int i = 0; i < points.size(); i++) {
			for (int j = i + 1; j < points.size(); j++) {
				this->CollisionCheck(i, j, dt);
			}
		}

		if (points.size() <= 1) {
			std::vector<Vec3> forces = ComputeForces();
			UpdatePositions(dt);
			UpdateVelocities(dt, forces);
		}		
	}
};