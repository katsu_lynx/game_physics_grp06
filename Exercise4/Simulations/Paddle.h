
#ifndef PADDLE_H
#define PADDLE_H
#include <GeometricPrimitive.h>
#include <Effects.h>
#include <PrimitiveBatch.h>
#include <VertexTypes.h>
#include <iostream>
#include "util/vectorbase.h"
#include "util/matrixbase.h"
#include "util/quaternion.h"
using namespace DirectX;


#define MYPRF(name, p) { std::cout << name << " " << XMVectorGetX(p) <<" "<< XMVectorGetY(p) <<" "<< XMVectorGetZ(p) <<" "<< XMVectorGetW(p) <<"\n"; }
#define MYPRM(name , m) { MYPRF(name, m.r[0]); MYPRF("    ", m.r[1]);  MYPRF("    ", m.r[2]);  MYPRF("    ", m.r[3]);  }

extern DirectX::BasicEffect* g_pEffectPositionNormal;
extern DirectX::PrimitiveBatch<DirectX::VertexPositionNormal>* g_pPrimitiveBatchPositionNormal;

class Paddle
{
public:

	Paddle(float radius, float angle, GamePhysics::Vec3 size);
	virtual ~Paddle();
	void movePaddle(float radius, float angle, float height);

	const float getRadius() const
	{
		return m_radius;
	}

	const float getAngle() const
	{
		return m_angle;
	}

	const float getHeight() const
	{
		return m_height;
	}

	GamePhysics::Vec3 m_center;
	GamePhysics::Quat m_rotation;
	GamePhysics::Vec3 m_scale;
	float m_radius;
	float m_angle;
	float m_height;

	GamePhysics::Mat4 m_objToWorld;
	GamePhysics::Mat4 m_worldToObj;
	GamePhysics::Mat4 m_scaledObjToWorld;
	GamePhysics::Mat4 m_worldToScaledObj;
};
#endif