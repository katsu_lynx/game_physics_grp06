#include "PingPongSimulator.h"

PingPongSimulator::PingPongSimulator()
{
	m_fMass      = 0.01f;
	m_fStiffness = 1.0f;
	m_fDamping   = 0.01f;
	m_iIntegrator = 1;
	m_iCube = 1;
	m_iTestCase = 1;
	m_externalForce = Vec3(0,0,0);
	m_pPingPongSystem = new PingPongSystem();
	m_score = 0;
}

const char * PingPongSimulator::getTestCasesStr()
{
	return "PingPong";
}

void PingPongSimulator::initUI(DrawingUtilitiesClass * DUC)
{
	this->DUC	 =	DUC;
	switch (m_iTestCase)
	{
	case 0:
		{
			TwAddVarRW(DUC->g_pTweakBar, "SpringCube", TW_TYPE_INT32, &m_iCube, "min = 1");
			TwType TW_TYPE_INTEGRATOR = TwDefineEnumFromString("Integrator", "Euler,LeapFrog,Midpoint");
			TwAddVarRW(DUC->g_pTweakBar, "Integrator", TW_TYPE_INTEGRATOR, &m_iIntegrator, "");
			TwAddVarRW(DUC->g_pTweakBar, "Mass",        TW_TYPE_FLOAT, &m_fMass,       "step=0.001  min=0.001");
			TwAddVarRW(DUC->g_pTweakBar, "Stiffness",   TW_TYPE_FLOAT, &m_fStiffness,  "step=0.001  min=0.001");
			TwAddVarRW(DUC->g_pTweakBar, "Damping",     TW_TYPE_FLOAT, &m_fDamping,    "step=0.001  min=0");
			TwAddVarRO(DUC->g_pTweakBar, "Score", TW_TYPE_INT32, &m_score, NULL);
		}
		break;
	default:
		break;
	}
}

void PingPongSimulator::reset(){
		m_mouse.x = m_mouse.y = 0;
		m_trackmouse.x = m_trackmouse.y = 0;
		m_oldtrackmouse.x = m_oldtrackmouse.y = 0;
	
}

void PingPongSimulator::notifyCaseChanged(int testCase)
{
	m_iTestCase = testCase;
	m_pPingPongSystem->SetCube(m_iCube);
	m_pPingPongSystem->SceneSetup(m_iTestCase);
}

void PingPongSimulator::externalForcesCalculations(float timeElapsed){
	Vec3 pullforce(0,0,0);
	if (m_trackmouse.x != 0 || m_trackmouse.y != 0)
	{
		Mat4 worldViewInv = Mat4(DUC->g_camera.GetWorldMatrix() * DUC->g_camera.GetViewMatrix());
		worldViewInv = worldViewInv.inverse();
		Vec3 forceView    = Vec3((float)m_trackmouse.x, (float)-m_trackmouse.y, 0);
		Vec3 forceWorld   =	worldViewInv.transformVectorNormal(forceView);
		float forceScale = 1.0f;
		pullforce =  pullforce + (forceWorld * forceScale);
		m_trackmouse.x= m_trackmouse.y = 0;
	}
	pullforce -=  pullforce * 5.0f * timeElapsed;
	// Gravity
	//Mat4 worldInv = Mat4(DUC->g_camera.GetWorldMatrix());
	//worldInv = worldInv.inverse();
	Vec3 gravity = Vec3(0, -9.81f, 0);// worldInv.transformVectorNormal(Vec3(0, -9.81f, 0));
	m_externalForce = gravity + pullforce;
	//m_externalForce = pullforce;
	//std::cout<<externalForce<<std::endl;
}

void PingPongSimulator::simulateTimestep(float timeStep)
{
	switch (m_iTestCase)
	{
	case 0:
		{
			
			//m_pPingPongSystem->SetGravity(m_externalForce);
			m_pPingPongSystem->SetMass(m_fMass);
			m_pPingPongSystem->SetStiffness(m_fStiffness);
			m_pPingPongSystem->SetDamping(m_fDamping);
	
			switch (m_iIntegrator)
			{
				case 0: m_pPingPongSystem->AdvanceEuler(timeStep);  m_pPingPongSystem->BoundingBoxCheck(); break;
				case 1: m_pPingPongSystem->AdvanceLeapFrog(timeStep); m_pPingPongSystem->BoundingBoxCheck(); break;
				case 2: m_pPingPongSystem->AdvanceMidPoint(timeStep); m_pPingPongSystem->BoundingBoxCheck(); break;
			}

			/*
			if (DXUTIsKeyDown(VK_LBUTTON))
				m_pPingPongSystem->dragTogether();
			*/
			m_pPingPongSystem->addGlobalFrameForce(m_externalForce);
			m_pPingPongSystem->update(timeStep);
			m_pPingPongSystem->handleKeys();
			m_pPingPongSystem->updateCollisions(timeStep);
			m_score=m_pPingPongSystem->getScore();
			if (m_score < 0) {
				m_pPingPongSystem->SceneSetup(0);
			}
		}
		break;
	default:
		break;
	}
}
void PingPongSimulator::drawFrame(ID3D11DeviceContext* pd3dImmediateContext)
{
	DUC->setUpLighting(Vec3(),0.4*Vec3(1,1,1),100,0.6*Vec3(0.83,0.36,0.36));
	
	//points
	auto& points = m_pPingPongSystem->GetPoints();
	int counter = 0;
	for (size_t i=0; i<points.size(); i++)
	{
		if (points[i].fixed) {
			//draw nothing
		}
		else if (points[i].striked) {
			DUC->setUpLighting(Vec3(), 0.4*Vec3(1, 1, 1), 100, 0.6*Vec3(0, 1, 0));
			DUC->drawTeapot(points[i].pos,Vec3 (0,0,0),Vec3 (0.2f,0.2f,0.2f));
			counter++;
		}
		else {
			DUC->setUpLighting(Vec3(), 0.4*Vec3(1, 1, 1), 100, 0.6*Vec3(1, 0, 0));
			DUC->drawTeapot(points[i].pos, Vec3(0, 0, 0), Vec3(0.2f, 0.2f, 0.2f));
		}
		
	}
	

	//ball
	if (counter == 20) {
		DUC->setUpLighting(Vec3(), 0.4*Vec3(1, 1, 1), 100, 0.6*Vec3(1, 1, 1));
		DUC->drawTeapot(this->m_pPingPongSystem->getBallPosition(), Vec3 (0, 0, 0), this->m_pPingPongSystem->getBallRadius());
	}
	else {
		DUC->setUpLighting(Vec3(), 0.4*Vec3(1, 1, 1), 100, 0.6*Vec3(1, 1, 1));
		DUC->drawSphere(this->m_pPingPongSystem->getBallPosition(), this->m_pPingPongSystem->getBallRadius());
	}
	counter = 0;
	//springs
	DUC->beginLine();
	auto& springs = m_pPingPongSystem->GetSprings();
	for(size_t i=0; i<springs.size(); i++)
	{
		Vec3 color = Vec3(0.5,0.5,0.5);
		Vec3 color2 = Vec3(1, 0, 0);
		if(points[springs[i].point2].striked){
			color2 = Vec3(0, 1, 0);
		}
		DUC->drawLine(points[springs[i].point1].pos,color,points[springs[i].point2].pos,color2);	
	}
	DUC->endLine();

	//rigidbodies
	Vec3 colors[7] = { Vec3(0,0,1), Vec3(0,1,0), Vec3(0,1,1), Vec3(1,0,0), Vec3(1,0,1), Vec3(1,1,0), Vec3(1,1,1) };
	int i = 0;
	for (RigidBody& rigidBody : m_pPingPongSystem->GetBodies())
	{
		DUC->setUpLighting(Vec3(0, 0, 0), 0.4*Vec3(1, 1, 1), 2000.0, colors[i % 3]);
		DUC->drawRigidBody(rigidBody.getObj2World());
		++i;
	}

	//paddles
	auto& paddles = m_pPingPongSystem->GetPaddles();
	std::mt19937 eng;
	std::uniform_real_distribution<float> randcol(0.0f, 1.0f);
	for (size_t i = 0; i<paddles.size(); i++)
	{
		DUC->setUpLighting(Vec3(), 0.4*Vec3(1, 1, 1), 100, colors[i % 7]);

		Vec3 color = Vec3(0, 0.4, 0);
		DUC->drawRigidBody(paddles[i].m_objToWorld);
	}



}

void PingPongSimulator::onClick(int x, int y)
{
	m_trackmouse.x += x - m_oldtrackmouse.x;
	m_trackmouse.y += y - m_oldtrackmouse.y;
}

void PingPongSimulator::onMouse(int x, int y)
{
	m_oldtrackmouse.x = x;
	m_oldtrackmouse.y = y;
}

void PingPongSimulator::setIntegrator(int integrator)
{
	m_iIntegrator = integrator;
}

void PingPongSimulator::setStiffness(float stiffness)
{
	m_fStiffness = stiffness;
	m_pPingPongSystem->SetStiffness(m_fStiffness);
}

void PingPongSimulator::setMass(float mass)
{
	m_fMass = mass;
	m_pPingPongSystem->SetMass(m_fMass);
}

void PingPongSimulator::setDampingFactor(float damping)
{
	m_fDamping = damping;
	m_pPingPongSystem->SetDamping(m_fDamping);
}

int PingPongSimulator::addMassPoint(Vec3 position, Vec3 velocity, bool isFixed)
{
	int index = m_pPingPongSystem->AddPoint(position,isFixed);
	m_pPingPongSystem->SetPointVelocity(index, velocity);
	return index;	
}

void PingPongSimulator::addSpring(int index1, int index2, float initialLength)
{
	 m_pPingPongSystem->AddSpring(index1,index2,initialLength);
}

int PingPongSimulator::getNumberOfMassPoints()
{
	return m_pPingPongSystem->GetPoints().size();
}

int PingPongSimulator::getNumberOfSprings()
{
	return m_pPingPongSystem->GetSprings().size();
}

Vec3 PingPongSimulator::getPositionOfMassPoint(int index)
{
	return m_pPingPongSystem->GetPoints()[index].pos;
}

Vec3 PingPongSimulator::getVelocityOfMassPoint(int index)
{
	return m_pPingPongSystem->GetPoints()[index].vel;
}

void PingPongSimulator::applyExternalForce(Vec3 force)
{
	m_externalForce = force;
	m_pPingPongSystem->SetGravity(m_externalForce.toDirectXVector());
}


//Rigid
int PingPongSimulator::getNumberOfRigidBodies()
{
	return m_pPingPongSystem->GetBodies().size();
}

Vec3 PingPongSimulator::getPositionOfRigidBody(int i)
{
	return m_pPingPongSystem->GetBodies()[i].getCenter();
}

Vec3 PingPongSimulator::getLinearVelocityOfRigidBody(int i)
{
	return m_pPingPongSystem->GetBodies()[i].getVelocity();
}

Vec3 PingPongSimulator::getAngularVelocityOfRigidBody(int i)
{
	return m_pPingPongSystem->GetBodies()[i].getAngularV();
}

void PingPongSimulator::applyForceOnBody(int i, Vec3 loc, Vec3 force)
{
	m_pPingPongSystem->GetBodies()[i].addForceWorld(force, loc);
}

void PingPongSimulator::addRigidBody(Vec3 position, Vec3 size, int mass)
{
	m_pPingPongSystem->GetBodies().emplace_back(position, size, mass);
	m_pPingPongSystem->GetBodies().back().update(0.0f);

}

void PingPongSimulator::setOrientationOf(int i, Quat orientation)
{
	m_pPingPongSystem->GetBodies()[i].setRotation(orientation);

}

void PingPongSimulator::setVelocityOf(int i, Vec3 velocity)
{
	m_pPingPongSystem->GetBodies()[i].setVelocity(velocity);
}