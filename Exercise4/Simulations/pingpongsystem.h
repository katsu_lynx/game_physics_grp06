#ifndef PINGPONGSYSTEM_H
#define PINGPONGSYSTEM_H

#include <vector>
#include "util\vectorbase.h"
#include "util\matrixbase.h"
#include "util\quaternion.h"
#include <iostream>
using namespace GamePhysics;
using std::cout;

#include "rigidBody.hpp"
#include "Paddle.h"
#include <AntTweakBar.h>
#define _USE_MATH_DEFINES
#include <cmath>


class PingPongSystem
{
public:
	
	struct Point
	{
		Vec3 pos;
		Vec3 vel;
		Vec3 radius;
		bool fixed;
		bool striked;
	};	

	struct Spring
	{
		int point1;
		int point2;
		float initialLength;
	};

	/*
	struct Paddle
	{
		float m_radius;
		float m_angle;
		float m_height;
		GamePhysics::Vec3 m_center;
		GamePhysics::Quat m_rotation;
		GamePhysics::Vec3 m_scale;

		GamePhysics::Mat4 m_objToWorld;
		GamePhysics::Mat4 m_worldToObj;
		GamePhysics::Mat4 m_scaledObjToWorld;
		GamePhysics::Mat4 m_worldToScaledObj;

	}; */
	

	PingPongSystem() : m_cube(0), m_rigidBodies() {}
	virtual ~PingPongSystem();

	void updateCollisions(float dt);

	int AddPoint(const Vec3& pos, bool fixed)
	{
		Point p;
		p.pos = pos;
		p.vel = Vec3(0,0,0);
		p.radius = (0.1f,0.1f,0.1f);
		p.fixed = fixed;
		p.striked = false;
		m_points.push_back(p);

		return (int)m_points.size()-1;
	}

	void AddSpring(int p1, int p2, float initialLength)
	{
		Spring s;
		s.point1 = p1;
		s.point2 = p2;
		s.initialLength = initialLength;
		m_springs.push_back(s);
	}

	void AddSpring(int p1, int p2)
	{
		Vec3 d = m_points[p1].pos - m_points[p2].pos;
		AddSpring(p1, p2, norm(d));
	}

	void AddRigidBody(Vec3 position, Vec3 size, int mass);

	//void AddPaddle(float radius, float angle, Vec3 size);
	//void MovePaddle(float radius, float angle, float height);

	void SetMass     (float mass     ) {m_mass      = mass     ;}
	void SetStiffness(float stiffness) {m_stiffness = stiffness;}
	void SetDamping  (float damping  ) {m_damping   = damping  ;}
	void SetCube	 (int   cube	 ) {m_cube = cube;			}


	void SetGravity  (const Vec3& gravity) {m_gravity = gravity;}

	const std::vector<Point>&  GetPoints()  {return m_points; }
	const std::vector<Spring>& GetSprings() {return m_springs;}
	std::vector<RigidBody>& GetBodies() { return m_rigidBodies; }
	std::vector<Paddle>& GetPaddles() { return m_paddles; }

	void AdvanceEuler(float dt);
	void AdvanceLeapFrog(float dt);
	void AdvanceMidPoint(float dt);

	void BoundingBoxCheck(float times = 1.0f);

	void SetPointVelocity(int p, const Vec3& vel){
		m_points[p].vel = vel;
	}

	void SetPointPosition(int p, const Vec3& pos){
		m_points[p].pos = pos;
	}

	void PrintPoint(size_t p){
		cout << "point " << p << " vel (" << m_points[p].vel.x << ", " << m_points[p].vel.y << ", " << m_points[p].vel.z 
			<< "), pos (" << m_points[p].pos.x << ", " << m_points[p].pos.y << ", " << m_points[p].pos.z << ") \n";
	}

	void SceneSetup(int sceneflag);

	void spawnBall();

	void rotatePointY(Point p, float phi) {
		float x = p.vel.x;
		float z = p.vel.z;
		p.vel = Vec3((cos(phi)*x + sin(phi)*z), 0, (cos(phi)*z - sin(phi)*x));
	}

	Vec3 getBallPosition() {return this->m_ball.pos;}
	Vec3 getBallRadius() { return this->m_ball.radius; }
	int getScore() { return m_score; }


	double calculateDistance(Vec3 vector);

	void dragTogether();
	void addGlobalFrameForce(const GamePhysics::Vec3 force);

	void update(float deltaTime);

	void handleKeys();
	

private:
	std::vector<Vec3> ComputeForces(float dt);
	void UpdatePositions(float dt);
	void UpdateVelocities(float dt, const std::vector<Vec3>& forces);

	std::vector<Point>  m_points;
	std::vector<Spring> m_springs;
	std::vector<RigidBody> m_rigidBodies;
	std::vector<Paddle> m_paddles;
	Point m_ball;
	RigidBody m_rigid_ball;

	float m_mass;
	float m_stiffness;
	float m_damping;

	int  m_cube;
	//saves the score of the player
	int m_score;
	Vec3 m_gravity;
};
#endif