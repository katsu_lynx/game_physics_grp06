#include "Paddle.h"


Paddle::Paddle(float radius, float angle, GamePhysics::Vec3 size) {
	m_radius = radius;
	m_angle = angle;
	m_height = 0.0f;
	m_center = GamePhysics::Vec3(radius*cos(angle), m_height, radius*sin(angle));
	m_rotation = GamePhysics::Quat(GamePhysics::Vec3(0.0f, 1.0f, 0.0f), -angle);
	m_scale = size;

	GamePhysics::Mat4 matrix;
	GamePhysics::Mat4 rotation = m_rotation.getRotMat();
	matrix.initTranslation(m_center.x, m_center.y, m_center.z);
	m_scaledObjToWorld = rotation * matrix;
	m_worldToScaledObj = m_scaledObjToWorld.inverse();
	matrix.initScaling(m_scale.x, m_scale.y, m_scale.z);
	m_objToWorld = matrix * m_scaledObjToWorld;
	m_worldToObj = m_objToWorld.inverse();
}

Paddle::~Paddle()
{
}

void Paddle::movePaddle(float radius, float angle, float height) {
	m_radius = radius;
	m_angle = angle;
	m_height = height;
	m_center = GamePhysics::Vec3(radius*cos(angle), m_height, radius*sin(angle));
	m_rotation = GamePhysics::Quat(GamePhysics::Vec3(0.0f, 1.0f, 0.0f), -angle);

	GamePhysics::Mat4 matrix;
	GamePhysics::Mat4 rotation = m_rotation.getRotMat();
	matrix.initTranslation(m_center.x, m_center.y, m_center.z);
	m_scaledObjToWorld = rotation * matrix;
	m_worldToScaledObj = m_scaledObjToWorld.inverse();
	matrix.initScaling(m_scale.x, m_scale.y, m_scale.z);
	m_objToWorld = matrix * m_scaledObjToWorld;
	m_worldToObj = m_objToWorld.inverse();
};
