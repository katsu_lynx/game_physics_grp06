﻿#include "SphereSystemSimulator.h"

std::function<float(float)> SphereSystemSimulator::m_Kernels[5] = {
	[](float x) {return 1.0f; },              // Constant, m_iKernel = 0
	[](float x) {return 1.0f - x; },          // Linear, m_iKernel = 1, as given in the exercise Sheet, x = d/2r
	[](float x) {return (1.0f - x)*(1.0f - x); }, // Quadratic, m_iKernel = 2
	[](float x) {return 1.0f / (x)-1.0f; },     // Weak Electric Charge, m_iKernel = 3
	[](float x) {return 1.0f / (x*x) - 1.0f; },   // Electric Charge, m_iKernel = 4
};

// SphereSystemSimulator member functions

SphereSystemSimulator::SphereSystemSimulator()
{
	m_fMass = 0.01f;
	m_fRadius = 0.02f;
	m_fNumber = 4;



	m_fDamping = 0.01f;
	m_iIntegrator = 1;
	m_iCube = 1;
	m_iTestCase = 1;
	m_iNaiivRender = false;
	m_iGridRender = false;
	m_externalForce = Vec3(0, 0, 0);
	m_pSphereSystem = new SphereSystem();
	m_pSphereSystem->SetNumber(m_fNumber);
	m_pSphereSystemGrid = new SphereSystem();
}

const char * SphereSystemSimulator::getTestCasesStr()
{
	return "Demo1, Demo2, Demo3";
}

void SphereSystemSimulator::initUI(DrawingUtilitiesClass * DUC)
{
	m_iNaiivRender = false;
	m_iGridRender = false;
	this->DUC = DUC;
	switch (m_iTestCase)
	{
	case 0:
	{
		//Demo 1
		TwAddVarRW(DUC->g_pTweakBar, "SpringCube", TW_TYPE_INT32, &m_iCube, "min = 1");
		TwAddVarRW(DUC->g_pTweakBar, "Mass", TW_TYPE_FLOAT, &m_fMass, "step=0.001  min=0.001");
		TwAddVarRW(DUC->g_pTweakBar, "Radius", TW_TYPE_FLOAT, &m_fRadius, "step=0.01  min=0.01");
		TwAddVarRW(DUC->g_pTweakBar, "Number", TW_TYPE_FLOAT, &m_fNumber, "step=1  min=0");
		TwAddVarRW(DUC->g_pTweakBar, "Damping", TW_TYPE_FLOAT, &m_fDamping, "step=0.001  min=0");
		TwType TW_TYPE_INTEGRATOR = TwDefineEnumFromString("Integrator", "Euler,LeapFrog,Midpoint");
		TwAddVarRW(DUC->g_pTweakBar, "Integrator", TW_TYPE_INTEGRATOR, &m_iIntegrator, "");
	}
	break;

	case 1:
	{
		//Demo 2
		TwAddVarRW(DUC->g_pTweakBar, "Mass", TW_TYPE_FLOAT, &m_fMass, "step=0.001  min=0.001");
		TwAddVarRW(DUC->g_pTweakBar, "Radius", TW_TYPE_FLOAT, &m_fRadius, "step=0.01  min=0.02");
		TwAddVarRW(DUC->g_pTweakBar, "Number", TW_TYPE_FLOAT, &m_fNumber, "step=1  min=0");
	}
	break;
	case 2:
	{
		//Demo 3
		m_iNaiivRender = true;
		m_iGridRender = true;
		//switch off and on rendering of the naiiv algorithm
		TwAddVarRW(DUC->g_pTweakBar, "Render Naiiv", TW_TYPE_BOOLCPP, &m_iNaiivRender, "");
		TwAddVarRW(DUC->g_pTweakBar, "Radius", TW_TYPE_FLOAT, &m_fRadius, "step=0.01  min=0.01");
		//switch off and on rendering of the grid algorithm
		TwAddVarRW(DUC->g_pTweakBar, "Render Grid", TW_TYPE_BOOLCPP, &m_iGridRender, "");

	}
	break;
	default:
		break;
	}
}

void SphereSystemSimulator::reset() {
	m_mouse.x = m_mouse.y = 0;
	m_trackmouse.x = m_trackmouse.y = 0;
	m_oldtrackmouse.x = m_oldtrackmouse.y = 0;
}

void SphereSystemSimulator::notifyCaseChanged(int testCase)
{
	m_iTestCase = testCase;
	m_pSphereSystem->SetCube(m_iCube);
	m_pSphereSystem->SceneSetup(m_iTestCase);

	m_pSphereSystemGrid->SetCube(m_iCube);
	m_pSphereSystemGrid->SceneSetup(m_iTestCase);
}

void SphereSystemSimulator::externalForcesCalculations(float timeElapsed) {
	Vec3 pullforce(0, 0, 0);
	if (m_trackmouse.x != 0 || m_trackmouse.y != 0)
	{
		Mat4 worldViewInv = Mat4(DUC->g_camera.GetWorldMatrix() * DUC->g_camera.GetViewMatrix());
		worldViewInv = worldViewInv.inverse();
		Vec3 forceView = Vec3((float)m_trackmouse.x, (float)-m_trackmouse.y, 0);
		Vec3 forceWorld = worldViewInv.transformVectorNormal(forceView);
		float forceScale = 1.0f;
		pullforce = pullforce + (forceWorld * forceScale);
		m_trackmouse.x = m_trackmouse.y = 0;
	}
	pullforce -= pullforce * 5.0f * timeElapsed;
	// Gravity
	//Mat4 worldInv = Mat4(DUC->g_camera.GetWorldMatrix());
	//worldInv = worldInv.inverse();
	Vec3 gravity = Vec3(0, -9.81f, 0);// worldInv.transformVectorNormal(Vec3(0, -9.81f, 0));
	m_externalForce = gravity + pullforce;
	//std::cout<<externalForce<<std::endl;
}

void SphereSystemSimulator::simulateTimestep(float timeStep)
{
	switch (m_iTestCase)
	{
	case 0: {

		m_pSphereSystem->SetMass(m_fMass);
		m_pSphereSystem->SetNumber(m_fNumber);
		m_pSphereSystem->SetRadius(m_fRadius);


		m_pSphereSystem->SetGravity(m_externalForce);
		m_pSphereSystem->SetDamping(m_fDamping);

		m_pSphereSystem->BruteForce(timeStep);
		switch (m_iIntegrator)
		{
			case 0: m_pSphereSystem->AdvanceEuler(timeStep); m_pSphereSystem->BoundingBoxCheck(); break;
			case 1: m_pSphereSystem->AdvanceLeapFrog(timeStep); m_pSphereSystem->BoundingBoxCheck(); break;
			case 2: m_pSphereSystem->AdvanceMidPoint(timeStep); m_pSphereSystem->BoundingBoxCheck(); break;
		}
		//BruteForce
		//m_pSphereSystem->SetGravity(m_externalForce);
		//m_pSphereSystem->BruteForce(timeStep, m_fRadius);
		//m_pSphereSystem->BoundingBoxCheck();
	}
	break;
	case 1: {
		//Grid
		m_pSphereSystem->SetMass(m_fMass);
		m_pSphereSystem->SetNumber(m_fNumber);
		m_pSphereSystem->SetRadius(m_fRadius);


		m_pSphereSystem->SetGravity(m_externalForce);
		m_pSphereSystem->SetDamping(m_fDamping);

		m_pSphereSystem->Grid(timeStep);
		switch (m_iIntegrator)
		{
			case 0: m_pSphereSystem->AdvanceEuler(timeStep); m_pSphereSystem->BoundingBoxCheck(); break;
			case 1: m_pSphereSystem->AdvanceLeapFrog(timeStep); m_pSphereSystem->BoundingBoxCheck(); break;
			case 2: m_pSphereSystem->AdvanceMidPoint(timeStep); m_pSphereSystem->BoundingBoxCheck(); break;
		}
	}
	break;
	case 2:	{
		//Both

		//Brute
		m_pSphereSystem->SetMass(m_fMass);
		m_pSphereSystem->SetNumber(m_fNumber);
		m_pSphereSystem->SetRadius(m_fRadius);


		m_pSphereSystem->SetGravity(m_externalForce);
		m_pSphereSystem->SetDamping(m_fDamping);

		m_pSphereSystem->BruteForce(timeStep);
		switch (m_iIntegrator)
		{
		case 0: m_pSphereSystem->AdvanceEuler(timeStep); m_pSphereSystem->BoundingBoxCheck(); break;
		case 1: m_pSphereSystem->AdvanceLeapFrog(timeStep); m_pSphereSystem->BoundingBoxCheck(); break;
		case 2: m_pSphereSystem->AdvanceMidPoint(timeStep); m_pSphereSystem->BoundingBoxCheck(); break;
		}

		//Grid
		m_pSphereSystemGrid->SetMass(m_fMass);
		m_pSphereSystemGrid->SetNumber(m_fNumber);
		m_pSphereSystemGrid->SetRadius(m_fRadius);


		m_pSphereSystemGrid->SetGravity(m_externalForce);
		m_pSphereSystemGrid->SetDamping(m_fDamping);

		m_pSphereSystemGrid->Grid(timeStep);
		switch (m_iIntegrator)
		{
		case 0: m_pSphereSystemGrid->AdvanceEuler(timeStep); m_pSphereSystemGrid->BoundingBoxCheck(); break;
		case 1: m_pSphereSystemGrid->AdvanceLeapFrog(timeStep); m_pSphereSystemGrid->BoundingBoxCheck(); break;
		case 2: m_pSphereSystemGrid->AdvanceMidPoint(timeStep); m_pSphereSystemGrid->BoundingBoxCheck(); break;
		}

	}
	break;
	default:
		break;
	}

	//TODO:Update Velocity und Position
	
}

void SphereSystemSimulator::drawFrame(ID3D11DeviceContext* pd3dImmediateContext)
{
	std::mt19937 eng;
	std::uniform_real_distribution<float> randcol(0.0f, 1.0f);
	auto& points = m_pSphereSystem->GetPoints();
	float pointSize = this->m_fRadius;

	if (m_iTestCase != 2) {
		for (size_t i = 0; i<points.size(); i++)
		{
			if (m_iNaiivRender) {
				break;
			}
			DUC->setUpLighting(Vec3(), 0.4*Vec3(1, 1, 1), 100, 0.6*Vec3(randcol(eng), randcol(eng), randcol(eng)));
			DUC->drawSphere(points[i].pos, Vec3(pointSize, pointSize, pointSize));
			
		}
	}
	

	if (m_iTestCase == 2) {
		auto& Gridpoints = m_pSphereSystemGrid->GetPoints();

		for (size_t i = 0; i<points.size(); i++)
		{
			if (!m_iNaiivRender) {
				break;
			}
			//red
			DUC->setUpLighting(Vec3(), 0.4*Vec3(1, 1, 1), 100, 0.6*Vec3(1.0f,0.0f,0.0f));
			DUC->drawSphere(points[i].pos, Vec3(pointSize, pointSize, pointSize));
		}
		
		for (size_t i = 0; i<points.size(); i++)
		{
			if (!m_iGridRender) {
				break;
			}
			//green
			DUC->setUpLighting(Vec3(), 0.4*Vec3(1, 1, 1), 100, 0.6*Vec3(0.0f, 1.0f, 0.0f));
			DUC->drawSphere(Gridpoints[i].pos, Vec3(pointSize, pointSize, pointSize));
		}
		
	}

}

void SphereSystemSimulator::onClick(int x, int y)
{
	m_trackmouse.x += x - m_oldtrackmouse.x;
	m_trackmouse.y += y - m_oldtrackmouse.y;
}

void SphereSystemSimulator::onMouse(int x, int y)
{
	m_oldtrackmouse.x = x;
	m_oldtrackmouse.y = y;
}

void SphereSystemSimulator::setMass(float mass)
{
	m_fMass = mass;
	m_pSphereSystem->SetMass(m_fMass);
}

void SphereSystemSimulator::setRadius(float radius)
{
	m_fRadius = radius;
	m_pSphereSystem->SetRadius(m_fRadius);
}

void SphereSystemSimulator::setNumber(int number)
{
	m_fNumber = number;
	m_pSphereSystem->SetNumber(m_fNumber);
}

void SphereSystemSimulator::setIntegrator(int integrator)
{
	m_iIntegrator = integrator;
}

void SphereSystemSimulator::setDampingFactor(float damping)
{
	m_fDamping = damping;
	m_pSphereSystem->SetDamping(m_fDamping);
	if (m_iTestCase == 2) {
		m_pSphereSystemGrid->SetDamping(m_fDamping);
	}
}

int SphereSystemSimulator::addMassPoint(Vec3 position, Vec3 velocity, bool isFixed)
{
	int index = m_pSphereSystem->AddPoint(position, isFixed);
	m_pSphereSystem->SetPointVelocity(index, velocity);
	if (m_iTestCase == 2) {
		index = m_pSphereSystemGrid->AddPoint(position, isFixed);
		m_pSphereSystemGrid->SetPointVelocity(index, velocity);
	}
	return index;
}

int SphereSystemSimulator::getNumberOfMassPoints()
{
	return m_pSphereSystem->GetPoints().size();
}

Vec3 SphereSystemSimulator::getPositionOfMassPoint(int index)
{
	return m_pSphereSystem->GetPoints()[index].pos;
}

Vec3 SphereSystemSimulator::getVelocityOfMassPoint(int index)
{
	return m_pSphereSystem->GetPoints()[index].vel;
}

void SphereSystemSimulator::applyExternalForce(Vec3 force)
{
	m_externalForce = force;
	m_pSphereSystem->SetGravity(m_externalForce.toDirectXVector());
	if (m_iTestCase == 2) {
		m_pSphereSystemGrid->SetGravity(m_externalForce.toDirectXVector());
	}
}