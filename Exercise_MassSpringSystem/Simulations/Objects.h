#pragma once
#include "Simulator.h"

class Masspoint {
public:
	Masspoint();
	Masspoint(Vec3 position, Vec3 velocity, bool isFixed);
	Masspoint(Vec3 position, Vec3 velocity, bool isFixed, float mass);
	void setPosition(Vec3 position);
	Vec3 getPosition();
	void setVelocity(Vec3 velocity);
	Vec3 getVelocity();
	void setAcceleration(Vec3 acceleration);
	Vec3 getAcceleration();
	void setIsFixed(bool fixed);
	bool getIsFixed();
	void setMass(float mass);
	float getMass();

private:
	Vec3 position;
	Vec3 velocity;
	Vec3 acceleration;
	bool isFixed;
	float mass;
};

class Spring {
public:
	Spring();
	Spring(Masspoint masspoint1, Masspoint masspoint2, float initalLength);
	Spring(Masspoint masspoint1, Masspoint masspoint2, float initalLength, float stifness);
	Spring(int massIndex1, int massIndex2, float initialLength);
	void setMasspoint1(Masspoint masspoint);
	Masspoint getMasspoint1();
	void setMasspoint2(Masspoint masspoint);
	Masspoint getMasspoint2();
	void setMassIndex1(int index);
	void setMassIndex2(int index);
	int getMassIndex1();
	int getMassIndex2();
	void setInitialLength(float initialLength);
	float getInitialLength();
	void setStiffness(float stiffness);
	float getStiffness();
private:
	Masspoint masspoint1;
	Masspoint masspoint2;
	int massIndex1;
	int massIndex2;
	float initialLength;
	float stiffness;
};