#include "MassSpringSystemSimulator.h"

#define GRAVITY = -9.81f;

MassSpringSystemSimulator::MassSpringSystemSimulator() {};

const char * MassSpringSystemSimulator::getTestCasesStr() {
	return "Demo1,Demo2,Demo3,Demo4,Demo5,Demo6";
}

void MassSpringSystemSimulator::initUI(DrawingUtilitiesClass * DUC)
{
	this->DUC = DUC;
	switch (m_iTestCase)
	{
	case 0:break;
	case 1:break;
	case 2:break;
	case 3:
		TwAddVarRW(DUC->g_pTweakBar, "Integration Method", TwDefineEnumFromString("Integration Method", "Euler,Leapfrog,Midpoint") , &m_iIntegrator, "");
		TwAddVarRW(DUC->g_pTweakBar, "Interaction on", TW_TYPE_BOOLCPP, &interaction, "");
		TwAddVarRW(DUC->g_pTweakBar, "Gravity on", TW_TYPE_BOOLCPP, &gravity, "");
		TwAddVarRW(DUC->g_pTweakBar, "Collisions on", TW_TYPE_BOOLCPP, &collisions, "");
		break;
	case 4:
		break;
	case 5:break;
	default:break;
	}
}

void MassSpringSystemSimulator::reset() {
	m_mouse.x = m_mouse.y = 0;
	m_trackmouse.x = m_trackmouse.y = 0;
	m_oldtrackmouse.x = m_oldtrackmouse.y = 0;
	this->notifyCaseChanged(this->m_iTestCase);
}

void MassSpringSystemSimulator::drawFrame(ID3D11DeviceContext* pd3dImmediateContext) {
	std::mt19937 eng;
	std::uniform_real_distribution<float> randCol(0.0f, 1.0f);
	for (Spring spring : this->springs) {
		Vec3 white = Vec3(1, 1, 1);
		Masspoint * masspoint1 = &this->masspoints.at(spring.getMassIndex1());
		Masspoint * masspoint2 = &this->masspoints.at(spring.getMassIndex2());
		DUC->beginLine();
		DUC->drawLine(masspoint1->getPosition(), white, masspoint2->getPosition(), white);
		DUC->endLine();
	}
	for (Masspoint masspoint : this->masspoints) {
		DUC->setUpLighting(Vec3(), 0.4*Vec3(1, 1, 1), 100, 0.6*Vec3(randCol(eng), randCol(eng), randCol(eng)));
		DUC->drawSphere(masspoint.getPosition(), Vec3(0.1f, 0.1f, 0.1f));
	}
}

void MassSpringSystemSimulator::notifyCaseChanged(int testCase) {
	this->m_iTestCase = testCase;
	this->masspoints.clear();
	this->springs.clear();
	switch (m_iTestCase)
	{
	case 0:
		this->gravity = false;
		this->collisions = false;
		this->interaction = false;
		this->m_iIntegrator = EULER;
		addMassPoint(Vec3(0, 0, 0), Vec3(-1, 0, 0), false, 10);
		addMassPoint(Vec3(0, 2, 0), Vec3(1, 0, 0), false, 10);
		addSpring(0, 1, 1);
		cout << "Demo1 !\n";

		break;
	case 1:
		this->m_iIntegrator = EULER;
		addMassPoint(Vec3(0, 0, 0), Vec3(-1, 0, 0), false, 10);
		addMassPoint(Vec3(0, 2, 0), Vec3(1, 0, 0), false, 10);
		addSpring(0, 1, 1);
		cout << "Demo2 !\n";
		break;
	case 2:
		this->m_iIntegrator = MIDPOINT;
		addMassPoint(Vec3(0, 0, 0), Vec3(-1, 0, 0), false, 10);
		addMassPoint(Vec3(0, 2, 0), Vec3(1, 0, 0), false, 10);
		addSpring(0, 1, 1);
		cout << "Demo3 !\n";
		break;
	case 3:
		addMassPoint(Vec3(0.2, -0.4, 0.2), Vec3(0, 0, 0), false, 10);
		addMassPoint(Vec3(-0.2, -0.4, 0.2), Vec3(0, 0, 0), false, 10);
		addMassPoint(Vec3(0.2, 0, 0.2), Vec3(0, 0, 0), false, 10);
		addMassPoint(Vec3(-0.2, 0, 0.2), Vec3(0, 0, 0), false, 10);
		addMassPoint(Vec3(0.2, 0.4, 0.2), Vec3(0, 0, 0), false, 10);
		addMassPoint(Vec3(-0.2, 0.4, 0.2), Vec3(0, 0, 0), false, 10);

		addMassPoint(Vec3(0.2, -0.4, -0.2), Vec3(0, 0, 0), false, 10);
		addMassPoint(Vec3(-0.2, -0.4, -0.2), Vec3(0, 0, 0), false, 10);
		addMassPoint(Vec3(0.2, 0, -0.2), Vec3(0, 0, 0), false, 10);
		addMassPoint(Vec3(-0.2, 0, -0.2), Vec3(0, 0, 0), false, 10);
		addMassPoint(Vec3(0.2, 0.4, -0.2), Vec3(0, 0, 0), false, 10);
		addMassPoint(Vec3(-0.2, 0.4, -0.2), Vec3(0, 0, 0), false, 10);
		for (int i = 0; i < 12; i++) {
			for (int j = 0; j < 12; j++) {
				if (j != i) {
					addSpring(i, j, 0.8);
				}
			}
		}
		this->gravity = true;
		this->collisions = true;
		this->interaction = true;
		cout << "Demo4 !\n";
		break;
	case 4:		
		cout << "Demo5 !\n";
		break;
	case 5:
		cout << "Demo6 !\n";
		break;
	default:
		cout << "Empty Test!\n";
		break;
	}
}

void MassSpringSystemSimulator::externalForcesCalculations(float timeElapsed) {
	Vec3 forces = Vec3(0, 0, 0);
	if (this->interaction && (m_trackmouse.x != 0 || m_trackmouse.y != 0)) {
		// Calcuate camera directions in world space
		Mat4 m = Mat4(DUC->g_camera.GetViewMatrix());
		m = m.inverse();
		Vec3 camRightWorld = Vec3(g_XMIdentityR0);
		camRightWorld = m.transformVectorNormal(camRightWorld);
		Vec3 camUpWorld = Vec3(g_XMIdentityR1);
		camUpWorld = m.transformVectorNormal(camUpWorld);

		// Add accumulated mouse deltas to movable object pos
		float speedScale = 10.0f;
		forces -= speedScale * (float)m_trackmouse.x * camRightWorld;
		forces += speedScale * (float)m_trackmouse.y * camUpWorld;

		// Reset accumulated mouse deltas
		m_trackmouse.x = m_trackmouse.y = 0;
	}

	for (int i = 0; i < masspoints.size(); i++) {
		Masspoint * masspoint = &masspoints.at(i);
		masspoint->setAcceleration(masspoint->getAcceleration() + this->m_externalForce / masspoint->getMass() + forces);
	}
	this->m_externalForce = Vec3(0,0,0);
}

void MassSpringSystemSimulator::applyExternalForce(Vec3 force) {
	this->m_externalForce += force;
}

void MassSpringSystemSimulator::calculateFloorCollision(Masspoint * masspoint) {
	if (this->collisions && masspoint->getPosition().y < -.95f) {
		masspoint->setPosition(masspoint->getPosition() * Vec3(1, 0, 1) + Vec3(0, -.95f, 0));
		masspoint->setVelocity(masspoint->getVelocity() * Vec3(1, -1/masspoint->getMass(), 1));
	}
}

void MassSpringSystemSimulator::doEuler(float timeStep) {
	externalForcesCalculations(timeStep);
	for (Spring spring : this->springs) {
		Masspoint * masspoint1 = &this->masspoints.at(spring.getMassIndex1());
		Masspoint * masspoint2 = &this->masspoints.at(spring.getMassIndex2());

		Vec3 vector = masspoint1->getPosition() - masspoint2->getPosition();
		float length = sqrt(vector.x * vector.x + vector.y * vector.y + vector.z * vector.z);
		float force = this->m_fStiffness * (length - spring.getInitialLength());

		masspoint1->setAcceleration(masspoint1->getAcceleration() + force*(masspoint1->getPosition() - masspoint2->getPosition()) / length);
		masspoint2->setAcceleration(masspoint2->getAcceleration() + force*(masspoint2->getPosition() - masspoint1->getPosition()) / length);
	}

	for (int i = 0; i < masspoints.size(); i++) {
		Masspoint * masspoint = &masspoints.at(i);
		if (!masspoint->getIsFixed()) {
			masspoint->setAcceleration((-masspoint->getAcceleration() + this->m_externalForce) / masspoint->getMass());
			masspoint->setVelocity(masspoint->getVelocity() + timeStep * masspoint->getAcceleration());
			masspoint->setPosition(masspoint->getPosition() + timeStep * masspoint->getVelocity());
		}
		else {
			masspoint->setVelocity(Vec3(0, 0, 0));
		}
		masspoint->setAcceleration(Vec3(0, 0, 0));
		this->calculateFloorCollision(masspoint);
	}
}

void MassSpringSystemSimulator::doMidpoint(float timeStep) {
	std::vector<Masspoint> tempPoints = std::vector<Masspoint>();
	externalForcesCalculations(timeStep);
	for (Spring spring : this->springs) {
		Masspoint * masspoint1 = &this->masspoints.at(spring.getMassIndex1());
		Masspoint * masspoint2 = &this->masspoints.at(spring.getMassIndex2());

		Vec3 vector = masspoint1->getPosition() - masspoint2->getPosition();
		float length = sqrt(vector.x * vector.x + vector.y * vector.y + vector.z * vector.z);
		float force = this->m_fStiffness * (length - spring.getInitialLength());

		masspoint1->setAcceleration(masspoint1->getAcceleration() + force*(masspoint1->getPosition() - masspoint2->getPosition()) / length);
		masspoint2->setAcceleration(masspoint2->getAcceleration() + force*(masspoint2->getPosition() - masspoint1->getPosition()) / length);
	}
	for (int i = 0; i < masspoints.size(); i++) {
		Masspoint * masspoint = &masspoints.at(i);
		Masspoint temp = Masspoint();
		temp.setMass(masspoint->getMass());
		if (!masspoint->getIsFixed()) {
			masspoint->setAcceleration((-masspoint->getAcceleration() + this->m_externalForce) / masspoint->getMass());
			masspoint->setVelocity(masspoint->getVelocity() + timeStep * masspoint->getAcceleration());

			temp.setPosition(masspoint->getPosition() + (timeStep / 2) * masspoint->getVelocity());
			temp.setVelocity(masspoint->getVelocity() + (timeStep / 2) * (masspoint->getAcceleration()));
		}
		tempPoints.push_back(temp);
	}
	for (Spring spring : this->springs) {
		Masspoint * masspoint1 = &tempPoints.at(spring.getMassIndex1());
		Masspoint * masspoint2 = &tempPoints.at(spring.getMassIndex2());

		Vec3 vector = masspoint1->getPosition() - masspoint2->getPosition();
		float length = sqrt(vector.x * vector.x + vector.y * vector.y + vector.z * vector.z);
		float force = this->m_fStiffness * (length - spring.getInitialLength());

		masspoint1->setAcceleration(masspoint1->getAcceleration() + (-force*(masspoint1->getPosition() - masspoint2->getPosition()) / length) / masspoint1->getMass());
		masspoint2->setAcceleration(masspoint2->getAcceleration() + (-force*(masspoint2->getPosition() - masspoint1->getPosition()) / length) / masspoint2->getMass());
	}
	for (int i = 0; i < masspoints.size(); i++) {
		Masspoint * masspoint = &masspoints.at(i);
		Masspoint * temp = &tempPoints.at(i);
		if (!masspoint->getIsFixed()) {
			masspoint->setPosition(masspoint->getPosition() + timeStep * temp->getVelocity());
			masspoint->setVelocity(masspoint->getVelocity() + timeStep * temp->getAcceleration());
		} else {
			masspoint->setVelocity(Vec3(0, 0, 0));
		}
		masspoint->setAcceleration(Vec3(0, 0, 0));
		this->calculateFloorCollision(masspoint);
	}
}

void leapfrogStep(Masspoint * masspoint, float timeStep) {
	// TODO
}

void MassSpringSystemSimulator::simulateTimestep(float timeStep) {
	
	if (this->gravity) {
		applyExternalForce(Vec3(0, this->m_fGravity, 0));
	}

	switch (this->m_iIntegrator) {
		case EULER:
			this->doEuler(timeStep);
			break;
		case MIDPOINT:
			this->doMidpoint(timeStep);
			break;
	}
}

void MassSpringSystemSimulator::onClick(int x, int y)
{
	m_trackmouse.x += x - m_oldtrackmouse.x;
	m_trackmouse.y += y - m_oldtrackmouse.y;
}

void MassSpringSystemSimulator::onMouse(int x, int y)
{
	m_oldtrackmouse.x = x;
	m_oldtrackmouse.y = y;
}

void MassSpringSystemSimulator::setIntegrator(int integrator) {
	this->m_iIntegrator = integrator;
}

void MassSpringSystemSimulator::setDampingFactor(float damping) {
	this->m_fDamping = damping;
}

void MassSpringSystemSimulator::setMass(float mass) {
	this->m_fMass = mass;
}

void MassSpringSystemSimulator::setStiffness(float stiffness) {
	this->m_fStiffness = stiffness;
}

int MassSpringSystemSimulator::addMassPoint(Vec3 position, Vec3 velocity, bool isFixed) {
	Masspoint masspoint = Masspoint(position, velocity, isFixed, this->m_fMass);
	this->masspoints.push_back(masspoint);

	return this->masspoints.size() - 1;
}

int MassSpringSystemSimulator::addMassPoint(Vec3 position, Vec3 velocity, bool isFixed, int mass) {
	Masspoint masspoint = Masspoint(position, velocity, isFixed, mass);
	this->masspoints.push_back(masspoint);

	return this->masspoints.size() - 1;
}

void MassSpringSystemSimulator::addSpring(int masspoint1, int masspoint2, float initialLength) {
	Spring spring = Spring(masspoint1, masspoint2, initialLength);
	this->springs.push_back(spring);
}

int MassSpringSystemSimulator::getNumberOfMassPoints() {
	return this->masspoints.size();
}

int MassSpringSystemSimulator::getNumberOfSprings() {
	return this->springs.size();
}

Vec3 MassSpringSystemSimulator::getPositionOfMassPoint(int index) {
	return this->masspoints.at(index).getPosition();
}

Vec3 MassSpringSystemSimulator::getVelocityOfMassPoint(int index) {
	return this->masspoints.at(index).getVelocity();
}