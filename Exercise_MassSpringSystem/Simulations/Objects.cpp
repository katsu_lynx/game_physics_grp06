#include "Objects.h"

// Masspoint
Masspoint::Masspoint() {};

Masspoint::Masspoint(Vec3 position, Vec3 velocity, bool isFixed) {
	this->position = position;
	this->velocity = velocity;
	this->isFixed = isFixed;
}

Masspoint::Masspoint(Vec3 position, Vec3 velocity, bool isFixed, float mass) {
	this->position = position;
	this->velocity = velocity;
	this->isFixed = isFixed;
	this->mass = mass;
}

bool Masspoint::getIsFixed() {
	return this->isFixed;
}

Vec3 Masspoint::getPosition() {
	return this->position;
}

Vec3 Masspoint::getVelocity() {
	return this->velocity;
}

Vec3 Masspoint::getAcceleration() {
	return this->acceleration;
}

float Masspoint::getMass() {
	return this->mass;
}

void Masspoint::setIsFixed(bool isFixed) {
	this->isFixed = isFixed;
}

void Masspoint::setPosition(Vec3 position) {
	this->position = position;
}

void Masspoint::setVelocity(Vec3 velocity) {
	this->velocity = velocity;
}

void Masspoint::setAcceleration(Vec3 acceleration) {
	this->acceleration = acceleration;
}

void Masspoint::setMass(float mass) {
	this->mass = mass;
}

// Spring
Spring::Spring() {};

Spring::Spring(Masspoint masspoint1, Masspoint masspoint2, float initialLength) {
	this->masspoint1 = masspoint1;
	this->masspoint2 = masspoint2;
	this->initialLength = initialLength;
}

Spring::Spring(Masspoint masspoint1, Masspoint masspoint2, float initialLength, float stiffness) {
	this->masspoint1 = masspoint1;
	this->masspoint2 = masspoint2;
	this->initialLength = initialLength;
	this->stiffness = stiffness;
}

Spring::Spring(int massIndex1, int massIndex2, float initialLength) {
	this->massIndex1 = massIndex1;
	this->massIndex2 = massIndex2;
	this->initialLength = initialLength;
}

float Spring::getInitialLength() {
	return this->initialLength;
}

Masspoint Spring::getMasspoint1() {
	return this->masspoint1;
}

Masspoint Spring::getMasspoint2() {
	return this->masspoint2;
}

float Spring::getStiffness() {
	return this->stiffness;
}

int Spring::getMassIndex1() {
	return this->massIndex1;
}

int Spring::getMassIndex2() {
	return this->massIndex2;
}

void Spring::setMasspoint1(Masspoint masspoint) {
	this->masspoint1 = masspoint;
}

void Spring::setMasspoint2(Masspoint masspoint) {
	this->masspoint2 = masspoint;
}

void Spring::setInitialLength(float initialLength) {
	this->initialLength = initialLength;
}

void Spring::setStiffness(float stiffness) {
	this->stiffness = stiffness;
}

void Spring::setMassIndex1(int index) {
	this->massIndex1 = index;
}

void Spring::setMassIndex2(int index) {
	this->massIndex2 = index;
}