#ifndef SPHERESYSTEM_H
#define SPHERESYSTEM_H

#include <vector>
#include "util\vectorbase.h"
#include <iostream>
using namespace GamePhysics;
using std::cout;

class SphereSystem
{
public:
	struct Point
	{
		Vec3 pos;
		Vec3 vel;
		bool fixed;
	};


	SphereSystem() : m_cube(0) {}

	int AddPoint(const Vec3& pos, bool fixed)
	{
		Point p;
		p.pos = pos;
		p.vel = Vec3(0, 0, 0);
		p.fixed = fixed;
		m_points.push_back(p);

		return (int)m_points.size() - 1;
	}

	void SetMass(float mass) { m_mass = mass; }
	void SetNumber(int number) { m_number = number; }
	void SetRadius(float radius) { m_radius = radius; }
	void SetDamping(float damping) { m_damping = damping; }
	void SetCube(int   cube) { m_cube = cube; }

	void SetGravity(const Vec3& gravity) { m_gravity = gravity; }

	const std::vector<Point>&  GetPoints() { return m_points; }

	void AdvanceEuler(float dt);
	void AdvanceLeapFrog(float dt);
	void AdvanceMidPoint(float dt);

	double calculateDistance(Point masspoint1, Point masspoint2);

	void BruteForce(float dt);
	void Grid(float dt);

	void BoundingBoxCheck(float times = 1.0f);
	void CollisionCheck(int i, int j, float dt);

	void SetPointVelocity(int p, const Vec3& vel) {
		m_points[p].vel = vel;
	}

	void SetPointPosition(int p, const Vec3& pos) {
		m_points[p].pos = pos;
	}

	void PrintPoint(size_t p) {
		cout << "point " << p << " vel (" << m_points[p].vel.x << ", " << m_points[p].vel.y << ", " << m_points[p].vel.z
			<< "), pos (" << m_points[p].pos.x << ", " << m_points[p].pos.y << ", " << m_points[p].pos.z << ") \n";
	}

	void SceneSetup(int sceneflag);

private:
	std::vector<Vec3> ComputeForces();
	void UpdatePositions(float dt);
	void UpdateVelocities(float dt, const std::vector<Vec3>& forces);

	std::vector<Point>  m_points;

	float m_mass;
	float m_radius;
	float m_damping;
	float m_lambda;

	int m_number;
	int m_cube;

	Vec3 m_gravity;
};
#endif